namespace :locations do

  # RAILS_ENV=production bundle exec rake locations:insert_es_cities
  desc 'Insert Cities'
  task :insert_es_cities => :environment do
    alava     = State.find_by slug: 'alava'
    alicante  = State.find_by slug: 'alicante-alacant'
    almeria   = State.find_by slug: 'almeria'
    bajadoz   = State.find_by slug: 'badajoz'
    barcelona = State.find_by slug: 'barcelona'
    caceres   = State.find_by slug: 'caceres'
    granada   = State.find_by slug: 'granada'
    guipuzcoa = State.find_by slug: 'gipuzkoa'
    malaga    = State.find_by slug: 'malaga'

    City.create(state_id: alava.id, code: "0", control_digit: "0", name: "Cigoitia", created_at: DateTime.now, updated_at: DateTime.now, slug: "cigoitia", banners_count: 0, state_name: alava.name, state_slug: alava.slug)
    City.create(state_id: alava.id, code: "0", control_digit: "0", name: "Ribera Alta", created_at: DateTime.now, updated_at: DateTime.now, slug: "ribera-alta", banners_count: 0, state_name: alava.name, state_slug: alava.slug)
    
    City.create(state_id: alicante.id, code: "0", control_digit: "0", name: "Guadalest", created_at: DateTime.now, updated_at: DateTime.now, slug: "guadalest", banners_count: 0, state_name: alicante.name, state_slug: alicante.slug)
    City.create(state_id: alicante.id, code: "0", control_digit: "0", name: "Hondón de las Nieves", created_at: DateTime.now, updated_at: DateTime.now, slug: "hondon-de-las-nieves", banners_count: 0, state_name: alicante.name, state_slug: alicante.slug)
    City.create(state_id: alicante.id, code: "0", control_digit: "0", name: "Jalón", created_at: DateTime.now, updated_at: DateTime.now, slug: "jalon", banners_count: 0, state_name: alicante.name, state_slug: alicante.slug)
    City.create(state_id: alicante.id, code: "0", control_digit: "0", name: "Muchamiel", created_at: DateTime.now, updated_at: DateTime.now, slug: "muchamiel", banners_count: 0, state_name: alicante.name, state_slug: alicante.slug)
    
    City.create(state_id: almeria.id, code: "0", control_digit: "0", name: "Balanegra", created_at: DateTime.now, updated_at: DateTime.now, slug: "balanegra", banners_count: 0, state_name: almeria.name, state_slug: almeria.slug)

    City.create(state_id: bajadoz.id, code: "0", control_digit: "0", name: "Guadiana del Caudillo", created_at: DateTime.now, updated_at: DateTime.now, slug: "guadiana-del-caudillo", banners_count: 0, state_name: bajadoz.name, state_slug: bajadoz.slug)

    City.create(state_id: barcelona.id, code: "0", control_digit: "0", name: "La Nou", created_at: DateTime.now, updated_at: DateTime.now, slug: "la-nou", banners_count: 0, state_name: barcelona.name, state_slug: barcelona.slug)

    City.create(state_id: caceres.id, code: "0", control_digit: "0", name: "Aldeacentenera", created_at: DateTime.now, updated_at: DateTime.now, slug: "aldeacentenera", banners_count: 0, state_name: caceres.name, state_slug: caceres.slug)
    City.create(state_id: caceres.id, code: "0", control_digit: "0", name: "Pueblonuevo de Miramontes", created_at: DateTime.now, updated_at: DateTime.now, slug: "pueblonuevo-de-miramontes", banners_count: 0, state_name: caceres.name, state_slug: caceres.slug)
    City.create(state_id: caceres.id, code: "0", control_digit: "0", name: "Tiétar", created_at: DateTime.now, updated_at: DateTime.now, slug: "tietar", banners_count: 0, state_name: caceres.name, state_slug: caceres.slug)

    City.create(state_id: granada.id, code: "0", control_digit: "0", name: "Dehesas Viejas", created_at: DateTime.now, updated_at: DateTime.now, slug: "dehesas-viejas", banners_count: 0, state_name: granada.name, state_slug: granada.slug)
    City.create(state_id: granada.id, code: "0", control_digit: "0", name: "Domingo Pérez de Granada", created_at: DateTime.now, updated_at: DateTime.now, slug: "domingo-perez-de-granada", banners_count: 0, state_name: granada.name, state_slug: granada.slug)
    City.create(state_id: granada.id, code: "0", control_digit: "0", name: "Játar", created_at: DateTime.now, updated_at: DateTime.now, slug: "jatar", banners_count: 0, state_name: granada.name, state_slug: granada.slug)
    City.create(state_id: granada.id, code: "0", control_digit: "0", name: "Valderrubio", created_at: DateTime.now, updated_at: DateTime.now, slug: "valderrubio", banners_count: 0, state_name: granada.name, state_slug: granada.slug)

    City.create(state_id: guipuzcoa.id, code: "0", control_digit: "0", name: "Ichaso", created_at: DateTime.now, updated_at: DateTime.now, slug: "ichaso", banners_count: 0, state_name: guipuzcoa.name, state_slug: guipuzcoa.slug)

    City.create(state_id: malaga.id, code: "0", control_digit: "0", name: "Montecorto", created_at: DateTime.now, updated_at: DateTime.now, slug: "montecorto", banners_count: 0, state_name: malaga.name, state_slug: malaga.slug)
  end

  # RAILS_ENV=production_mx bundle exec rake locations:insert_mx_cities
  desc 'Insert MX Cities'
  task :insert_mx_cities => :environment do

    aguascalientes = State.find_by slug: 'aguascalientes'
    baja_california = State.find_by slug: 'baja-california'
    baja_california_sur = State.find_by slug: 'baja-california-sur'
    campeche   = State.find_by slug: 'campeche'
    chihuahua  = State.find_by slug: 'chihuahua'
    chiapas    = State.find_by slug: 'chiapas'
    coahuila   = State.find_by slug: 'coahuila-de-zaragoza'
    colima     = State.find_by slug: 'colima'
    durango    = State.find_by slug: 'durango'
    guanajuato = State.find_by slug: 'guanajuato'
    guerrero   = State.find_by slug: 'guerrero'
    hidalgo    = State.find_by slug: 'hidalgo'
    jalisco    = State.find_by slug: 'jalisco'
    estado_de_mexico = State.find_by slug: 'mexico'
    michoacan = State.find_by slug: 'michoacan-de-ocampo'
    morelos   = State.find_by slug: 'morelos'
    nayarit   = State.find_by slug: 'nayarit'
    nuevo_leon = State.find_by slug: 'nuevo-leon'
    oaxaca     = State.find_by slug: 'oaxaca'
    puebla     = State.find_by slug: 'puebla'
    queretaro  = State.find_by slug: 'queretaro'
    quintana_roo = State.find_by slug: 'quintana-roo'
    san_luis_potosi = State.find_by slug: 'san-luis-potosi'
    sinaloa    = State.find_by slug: 'sinaloa'
    sonora     = State.find_by slug: 'sonora'
    tabasco    = State.find_by slug: 'tabasco'
    tamaulipas = State.find_by slug: 'tamaulipas'
    tlaxcala   = State.find_by slug: 'tlaxcala'
    veracruz   = State.find_by slug: 'veracruz-de-ignacio-de-la-llave'
    yucatan    = State.find_by slug: 'yucatan'
    zacatecas  = State.find_by slug: 'zacatecas'

    
    City.create(state_id: aguascalientes.id, code: "0", control_digit: "0", name: "Palo Alto", created_at: DateTime.now, updated_at: DateTime.now, slug: "palo-alto", banners_count: 0, state_name: aguascalientes.name, state_slug: aguascalientes.slug)
    
    City.create(state_id: baja_california.id, code: "0", control_digit: "0", name: "Rosarito", created_at: DateTime.now, updated_at: DateTime.now, slug: "Rosarito", banners_count: 0, state_name: baja_california.name, state_slug: baja_california.slug)
    
    baja_california_sur_array = ["Ciudad Constitución", "Santa Rosalía", "San José del Cabo"]
    baja_california_sur_array.each do |city|
      City.create(state_id: baja_california_sur.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: baja_california_sur.name, state_slug: baja_california_sur.slug)
    end
    
    campeche_array = ["San Francisco de Campeche", "Ciudad del Carmen", "Xpujil"]
    campeche_array.each do |city|
      City.create(state_id: campeche.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: campeche.name, state_slug: campeche.slug)
    end

    chihuahua_array = ["Miguel Ahumada", "Juan Aldama", "Valle de Ignacio Allende", "Santa Eulalia", "Mariano Balleza", "Batopilas", "San Buenaventura", "Santa Rosalía de Camargo", "José Esteban Coronado", "Santiago de Coyame", "Chínipas de Almada", "San Lorenzo", "Hermenegildo Galeana", "Valentín Gómez Farías", "San Nicolás de Carretas", "Témoris", "Vicente Guerrero", "José Mariano Jiménez", "Octaviano López", "Mariano Matamoros", "Pedro Meoqui", "Melchor Ocampo", "Manuel Ojinaga", "San Andrés", "Santa Cruz de Rosales", "Valle del Rosario", "San Francisco Javier de Satevó"]
    chihuahua_array.each do |city|
      City.create(state_id: chihuahua.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: chihuahua.name, state_slug: chihuahua.slug)
    end

    chiapas_array = ["Jaltenango de la Paz", "Rivera el Viejo Carmen", "San Andrés Larráinzar", "Metapa de Domínguez"]
    chiapas_array.each do |city|
      City.create(state_id: chiapas.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: chiapas.name, state_slug: chiapas.slug)
    end

    coahuila_array = ["Ciudad Acuña", "Cuatro Ciénegas de Carranza", "Ciudad Melchor Múzquiz", "Parras de la Fuente", "Nueva Rosita"]
    coahuila_array.each do |city|
      City.create(state_id: coahuila.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: coahuila.name, state_slug: coahuila.slug)
    end

    colima_array = ["Ciudad de Armería"]
    colima_array.each do |city|
      City.create(state_id: colima.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: colima.name, state_slug: colima.slug)
    end
    
    durango_array = ["Ciudad Canatlán", "Cuencamé de Ceniceros", "Victoria de Durango", "Ciudad Guadalupe Victoria", "Villa Hidalgo", "Ciudad Lerdo", "San Francisco del Mezquital", "Villa Ocampo", "Santa María del Oro", "Francisco I. Madero", "Ciudad Villa Unión", "El Salto", "Tayoltita", "San Juan del Río del Centauro del Norte", "Tamazula de Victoria", "Santa Catarina de Tepehuanes", "Tlahualilo de Zaragoza", "Ciudad Vicente Guerrero"]
    durango_array.each do |city|
      City.create(state_id: durango.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: durango.name, state_slug: durango.slug)
    end

    guanajuato_array = ["Ciudad Manuel Doblado", "León de los Aldama""Silao"]
    guanajuato_array.each do |city|
      City.create(state_id: guanajuato.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: guanajuato.name, state_slug: guanajuato.slug)
    end

    guerreo_array = ["Ciudad Apaxtla de Castrejón", "San Jerónimo de Juárez", "Coahuayutla de Guerrero", "Cruz Grande", "Acapetlahuaya", "Tlacotepec", "Ciudad de Huitzuco", "Zihuatanejo", "Tierra Colorada", "Chichihualco", "Apango", "Ixcapuzalco", "Ciudad Altamirano", "Tlalixtaquilla", "La Unión", "Zirándaro de los Chávez", "Zumpango del Río", "Hueycantenango"]
    guerreo_array.each do |city|
      City.create(state_id: guerrero.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: guerrero.name, state_slug: guerrero.slug)
    end
    
    hidalgo_array = ["Jacala", "Juárez", "Metzquititlán", "Mixquiahuala", "Molango", "Nopala", "Orizatlán", "Progreso", "Pachuquilla", "Santiago Tulantepec", "Tezontepec", "Tulancingo", "Zacualtipán"]
    hidalgo_array.each do |city|
      City.create(state_id: hidalgo.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: hidalgo.name, state_slug: hidalgo.slug) 
    end

    jalisco_array = ["Autlán de la Grana", "El Tuito", "Ciudad Guzmán", "San Sebastián del Sur"]
    jalisco_array.each do |city|
      City.create(state_id: jalisco.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: jalisco.name, state_slug: jalisco.slug)
    end

    estado_de_mexico_array = ["Acolman de Nezahualcóyotl", "Aculco de Espinosa", "Villa de Almoloya de Juárez", "Amanalco de Becerra", "Amecameca de Juárez", "Apaxco de Ocampo", "San Salvador Atenco", "Santa Cruz Atizapán", "Ciudad López Mateos", "Atlacomulco de Fabela", "Atlautla de Victoria", "Ayapango de Gabriel Ramos Millán", "Calimaya de Díaz González", "Capulhuac de Mirafuentes", "San Francisco Coacalco", "Chalco de Díaz Covarrubias", "Chicoloapan de Juárez", "Chiconcuac de Juárez", "Villa de Donato Guerra", "Ecatzingo de Hidalgo", "Huixquilucan de Degollado", "Tlazala de Fabela", "Ixtlahuaca de Rayón", "Jilotepec de Molina Enríquez", "Santa Ana Jilotzingo", "Juchitepec de Mariano Rivapalacio", "Lerma de Villada", "San Mateo Mexicaltzingo", "San Bartolo Morelos", "Santa Ana Nextlalpan", "Villa Nicolás Romero", "Ocuilan de Arteaga", "El Oro de Hidalgo", "Otumba de Gómez Farías", "Villa Cuauhtémoc", "Ozumba de Alzate", "Los Reyes Acaquilpan", "Polotitlán de la Ilustración", "Santa María Rayón", "Santo Tomás de los Plátanos", "San Francisco Soyaniquilpan", "Sultepec de Pedro Ascencio de Alquisiras", "Tecámac de Felipe Villanueva", "Tejupilco de Hidalgo", "Temascalcingo de José María Velasco", "Temascaltepec de González", "Tenancingo de Degollado", "Tenango de Arista", "Teotihuacán de Arista", "Tepetlaoxtoc de Hidalgo", "Santiago Tequixquiac", "San Mateo Texcalyacac", "Texcoco de Mora", "Santiago Tianguistenco de Galeana", "San Andrés Timilpan", "Tlalmanalco de Velázquez", "Tlalnepantla", "Toluca de Lerdo", "Tultitlán de Mariano Escobedo", "San José Villa de Allende", "Real de Minas Zacualpan", "San Miguel Zinacantepec", "Zumpahuacán", "Zumpango de Ocampo", "Cuautitlán Izcalli", "Xico", "Villa Luvianos", "Santa María Tonanitla"] 
    estado_de_mexico_array.each do |city|
      City.create(state_id: estado_de_mexico.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: estado_de_mexico.name, state_slug: estado_de_mexico.slug)
    end
    
    michoacan_arary = ["Acuitzio del Canje", "Angamacutiro de la Unión", "Mineral de Angangueo", "Apatzingán de la Constitución", "Ario de Rosales", "Briseñas de Matamoros", "Buenavista Tomatlán", "Carácuaro de Morelos", "Coahuayana de Hidalgo", "Coeneo de la Libertad", "Copándaro de Galeana", "Cotija de la Paz", "Cuitzeo del Porvenir", "Churumucho de Morelos", "Lombardía", "Ciudad Hidalgo", "Huaniqueo de Morales", "Huetamo de Núñez", "Ixtlán de los Hervores", "Jacona de Plancarte", "Villa Jiménez", "Jiquilpan de Juárez", "Benito Juárez", "Jungapeo de Juárez", "Villa Madero", "Maravatío de Ocampo", "San José de Gracia", "Ciudad Lázaro Cárdenas", "Villa Morelos", "Nueva Italia de Ruiz", "Nocupétaro de Morelos", "Nuevo San Juan Parangaricutiro", "Paracho de Verduzco", "Penjamillo de Degollado", "Peribán de Ramos", "La Piedad de Cabadas", "Purépero de Echáiz", "Cojumatlán de Régules", "Los Reyes de Salgado", "Sahuayo de Morelos", "Santa Clara del Cobre", "Susupuato de Guerrero", "Tacámbaro de Collados", "Santiago Tangamandapio", "Tangancícuaro de Arista", "Tanhuato de Guerrero", "Tiquicheo", "Tlalpujahua de Rayón", "Tumbiscatío de Ruiz", "Uruapan del Progreso", "San Pedro Cahro", "Vista Hermosa de Negrete", "Zamora de Hidalgo", "Zinapécuaro de Figueroa", "Heroica Zitácuaro", "Pastor Ortiz"]
    michoacan_arary.each do |city|
      City.create(state_id: michoacan.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: michoacan.name, state_slug: michoacan.slug)
    end

    morelos_array = ["Ciudad Ayala", "Yautepec de Zaragoza", "Zacatepec de Hidalgo", "Zacualpan de Amilpas"]
    morelos_array.each do |city|
      City.create(state_id: morelos.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: morelos.name, state_slug: morelos.slug)
    end

    nayarit_array = ["Jesús María", "Valle de Banderas"]
    nayarit_array.each do |city|
      City.create(state_id: nayarit.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: nayarit.name, state_slug: nayarit.slug)
    end

    nuevo_leon_array = ["Ciudad Apodaca", "Ciudad Cerralvo", "Ciudad Benito Juárez", "Lampazos de Naranjo", "Ciudad Sabinas Hidalgo", "Ciudad Santa Catarina", "Ciudad de Villaldama"]
    nuevo_leon_array.each do |city|
      City.create(state_id: nuevo_leon.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: nuevo_leon.name, state_slug: nuevo_leon.slug)
    end

    oaxaca_array = ["Temascal"]
    oaxaca_array.each do |city|
      City.create(state_id: oaxaca.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: oaxaca.name, state_slug: oaxaca.slug)
    end

    puebla_array = ["San José Acateno", "Acatlán de Osorio", "Acatzingo de Hidalgo", "Ciudad de Ajalpan", "Acaxtlahuacán de Albino Zertuche", "Aljojuca", "Altepexi", "Amixtlán", "Amozoc de Mota", "San Martín Atexcal", "Atlixco", "Santiago Atzitzihuacán", "Atzitzintla", "San Andrés Calpan", "Santa María Cohetzala", "Santa María Coronango", "Santa María Coyomeapan", "San Vicente Coyotepec", "San Esteban Cuautempan", "San Juan Cuautlancingo", "San Pedro Cuayuca", "Ciudad de Cuetzalan", "Ciudad Serdán", "Chiautla de Tapia", "San Lorenzo Chiautzingo", "Chila Honey", "San Juan Epatlán", "Metlaltoyuca", "San Pablo de las Tunas", "Bienvenido", "Huitzilan", "Santa Clara Huitziltepec", "Ixcamilpa", "San Juan Ixcaquixtla", "Cuanalá", "Nuevo Necaxa", "Atenayuca", "Saltillo", "Ciudad de Libres", "San Francisco Mixtla", "Morelos Cañada", "San Buenaventura Nealtican", "Nopalucan de la Granja", "Santa Clara Ocoyucan", "Pahuatlán de Valle", "Palmar de Bravo", "Heroica Puebla de Zaragoza", "Tochimiltzingo", "Tepeaca de Negrete", "San Felipe Tepemaxalco", "Ciudad de Tlatlauquitepec", "Tulcingo de Valle", "Santa María del Monte", "Xicotepec de Juárez", "San Juan Xiutetelco", "Cinco de Mayo", "Xochitlán de Romero Rubio", "Xochitlán", "Zacatlán de las Manzanas", "Zapotitlán Salinas", "Santiago Zautla", "San Sebastián Zinacatepec"]
    puebla_array.each do |city|
      City.create(state_id: puebla.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: puebla.name, state_slug: puebla.slug)
    end

    queretaro_array = ["Amealco", "Cadereyta", "El Pueblito", "La Cañada", "Santiago de Querétaro"]
    queretaro_array.each do |city|
      City.create(state_id: queretaro.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: queretaro.name, state_slug: queretaro.slug)
    end

    quintana_roo_array = ["Chetumal", "Cancún", "Kantunilkín", "Playa del Carmen"]
    quintana_roo_array.each do |city|
      City.create(state_id: quintana_roo.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: quintana_roo.name, state_slug: quintana_roo.slug)
    end

    san_luis_potosi_array = ["Ahualulco del Sonido 13", "Real de Catorce", "Salinas de Hidalgo"]
    san_luis_potosi_array.each do |city|
      City.create(state_id: san_luis_potosi.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: san_luis_potosi.name, state_slug: san_luis_potosi.slug)
    end

    sinaloa_array = ["Los Mochis", "Culiacán Rosales", "La Cruz", "Escuinapa de Hidalgo", "El Rosario", "Guamúchil", "Sinaloa de Leyva"]
    sinaloa_array.each do |city|
      City.create(state_id: sinaloa.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: sinaloa.name, state_slug: sinaloa.slug)
    end

    sonora_array = ["Heroica Caborca", "Ciudad Obregón", "Heroica Ciudad de Cananea", "Heroica Guaymas de Zaragoza", "Magdalena de Kino", "Heroica Nogales", "Heroica Ciudad de Ures", "Villa Pesqueira (Mátape)", "Sonoyta", "Villa Juárez"]
    sonora_array.each do |city|
      City.create(state_id: sonora.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: sonora.name, state_slug: sonora.slug)
    end

    tabasco_array = ["Balancán de Domínguez", "Heroica Cárdenas", "Frontera", "Villahermosa", "Tenosique de Pino Suárez"]
    tabasco_array.each do |city|
      City.create(state_id: tabasco.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: tabasco.name, state_slug: tabasco.slug)
    end

    tamaulipas_array = ["Ciudad Camargo", "Nueva Ciudad Guerrero", "Santander Jiménez", "Llera de Canales", "Villa Mainero", "Ciudad Mante", "Heroica Matamoros", "Ciudad Mier", "Ciudad Miguel Alemán", "Nueva Villa de Padilla", "Ciudad Río Bravo", "Ciudad Victoria"]
    tamaulipas_array.each do |city|
      City.create(state_id: tamaulipas.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: tamaulipas.name, state_slug: tamaulipas.slug)
    end

    tlaxcala_array = ["Apetatitlán", "Santa Ana Chiautempan", "Muñoz", "Villa Mariano Matamoros", "Mazatecochco", "Contla", "Tepetitla", "Sanctórum", "Ciudad de Nanacamilpa", "Acuamanala", "Villa Vicente Guerrero", "Tetla", "Tlaxcala de Xicohténcatl", "San Juan Totolac", "Zitlaltepec", "San Cosme Xalostoc", "Papalotla", "San Dionisio Yauhquemecan"]
    tlaxcala_array.each do |city|
      City.create(state_id: tlaxcala.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: tlaxcala.name, state_slug: tlaxcala.slug)
    end

    veracruz_array = ["Alto Lucero", "Naranjos", "José Cardel", "Ciudad Mendoza", "Tamarindo", "Progreso de Zaragoza", "Cosamaloapan", "Cosautlán de Carvajal", "Coscomatepec de Bravo", "Chicontepec de Tejeda", "Dos Ríos", "Fortín de las Flores", "Huatusco de Chicuellar", "Xalapa-Enríquez", "Jáltipan de Morelos", "Naolinco de Victoria", "Papantla de Olarte", "Tempoal de Sánchez", "Túxpam de Rodríguez Cano", "Colonia Manuel González", "Tatahuicapan", "La Chinantla", "Xochiapa"]
    veracruz_array.each do |city|
      City.create(state_id: veracruz.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: veracruz.name, state_slug: veracruz.slug)
    end

    yucatan_array = ["Suma de Hidalgo"]
    yucatan_array.each do |city|
      City.create(state_id: yucatan.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: yucatan.name, state_slug: yucatan.slug)
    end

    zacatecas_array =["Florencia", "Víctor Rosales", "San Pedro Piedra Gorda", "Nieves", "Jerez de García Salinas"]
    zacatecas_array.each do |city|
      City.create(state_id: zacatecas.id, code: "0", control_digit: "0", name: city, created_at: DateTime.now, updated_at: DateTime.now, slug: city.parameterize, banners_count: 0, state_name: zacatecas.name, state_slug: zacatecas.slug)
    end
  end

end

