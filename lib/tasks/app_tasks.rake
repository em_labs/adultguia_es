# encoding: utf-8
namespace :app do

  desc "Load sample banners"
  task :load_sample_banners => :environment do
    cats = BannerCategory.all.pluck(:id)
    dates = (90.days.ago.to_date..Date.current).to_a
    user = User.first

    # validates :geo_address, :geo_lat, :geo_lon, :geo_keywords, :geo_place_id, presence: true

    50.times do
      Banner.create(
        user: user,
        banner_category_id: cats.shuffle.last,
        title: Faker::Lorem.sentence,
        description: Faker::Lorem.paragraph,
        email: Faker::Internet.email,
        phone: rand(100000000..999999999),
        sort_date: dates.shuffle.last,
        # sample geo
        geo_address: "Calle Alfafar, Valencia, España",
        geo_lat: "39.42535059999999",
        geo_lon: "-0.36311449999993783",
        geo_keywords: "Calle Alfafar, Valencia, España, 46013, Comunidad Valenciana, L'Horta de València, Poblados del Sur, Castellar-Oliveral, Valencian Community",
        geo_place_id: "ChIJK5A1XzhJYA0R1_qF-6pdLZg",
      )
    end
  end

  desc "Reset all users pass"
  task :reset_pass => :environment do
    User.all.each{|u| u.password = 'asdasdasd'; u.save }
  end

  desc "Import states"
  task :seed_states => :environment do
    locale = Rails.configuration.x.app_country
    spreadsheet = Roo::Spreadsheet.open("#{Rails.root}/db/data/#{locale}/provincias.csv")
    header = spreadsheet.row(1)

    count = 0
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      State.create(
        id: row['id'].to_i,
        name: row['name']
      )
    end
  end

  desc "Import Cities"
  task :seed_cities => :environment do
    locale = Rails.configuration.x.app_country
    spreadsheet = Roo::Spreadsheet.open("#{Rails.root}/db/data/#{locale}/municipios.csv")
    header = spreadsheet.row(1)

    count = 0
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      City.create(
        state_id: row['state_id'].to_i,
        name: row['name'],
        code: row['code'],
        control_digit: row['control_digit'],
      )
    end
  end

  desc "Set banners location (migration)"
  task :set_banners_location => :environment do
    Banner.all.each do |bb|
      bb.state = State.all.to_a.shuffle.first
      bb.city = bb.state.cities.to_a.shuffle.first
      bb.save
    end
    City.all.each do |cc|
      City.reset_counters(cc.id, :banners)
    end
  end

  desc "Auto Subir anuncios viejos"
  task :auto_top_old_posts => :environment do
    if AppSetting.get('auto_subir_old_posts') == 'true'
      last = Banner.unscoped.non_deleted.visible.order('sort_date asc').limit(20)
      banner = last.to_a.shuffle.first
      banner.update_column :sort_date, Time.now
      puts "Banner ID #{banner.id} moved to top"
    end
  end

  # RAILS_ENV=production bundle exec rake app:rename_places
  desc 'Rename some places'
  task :rename_places => :environment do
    alava = State.find_by name: 'Araba/Álava'
    alava.update_attributes(name: 'Álava', slug: 'Álava'.parameterize)

    alava = State.find_by name: 'Balears, Illes'
    alava.update_attributes(name: 'Baleares', slug: 'Baleares'.parameterize)
  end

  # RAILS_ENV=production_mx bundle exec rake app:rename_mx_df
  desc 'Rename some places'
  task :rename_mx_df => :environment do
    df = State.find_by name: 'Distrito Federal'
    df.update_attributes(name: 'Distrito Federal DF', slug: 'Distrito Federal DF'.parameterize)
  end

  desc 'Paid Location banners'
  task :create_paid_banner_locations_from_top_banners => :environment do
    Banner.unscoped.top.each do |banner|
      puts "#{banner.id} - #{banner.title}"
      PaidBannerLocation.create(
        user_id:   banner.user_id,
        banner_id: banner.id,
        state_id:  banner.state_id,
        city_id:   banner.city_id,
        banner_category_id: banner.banner_category_id
      )
    end
  end

end
