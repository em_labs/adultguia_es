class NotificationsMailerPreview < ActionMailer::Preview
  
  def new_feedback
    NotificationsMailer.new_feedback(FeedbackRequest.last)
  end

end