class BannerMailerPreview < ActionMailer::Preview
  
  def new_contact_request
    BannerMailer.new_contact_request(BannerContactRequest.last)
  end

  def send_to_friend
    BannerMailer.send_to_friend(SentFavorite.last)
  end

  def new_phone_view
    BannerMailer.new_phone_view(Banner.last)
  end

end