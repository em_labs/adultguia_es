# Based on production defaults
require Rails.root.join("config/environments/production")

Rails.application.configure do

  config.time_zone = 'Mexico City'
  config.x.app_country = 'mx'

  config.action_mailer.default_url_options = { :host => "mx.adultguia.com" }
  config.action_mailer.asset_host = "https://mx.adultguia.com"
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.smtp_settings = {
    :user_name => "adultguiamail",
    :password => "AdultGuia2017",
    :domain => "adultguia.com",
    :address => 'smtp.sendgrid.net',
    :port => 587,
    :authentication => :plain,
    :enable_starttls_auto => true
  }

end