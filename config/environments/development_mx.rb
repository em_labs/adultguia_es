require Rails.root.join("config/environments/development")

Rails.application.configure do
  config.i18n.default_locale = 'es-MX'
  config.x.app_country = 'mx'
end
