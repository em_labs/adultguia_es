# quito la verificacion de SSL en dev
# para maco: SSL_connect returned=1 errno=0 state=SSLv3 read server certificate B: certificate verify failed
# if Rails.env.development?
#   OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
# end

Geocoder.configure(

  # geocoding service (see below for supported options):
  :lookup => :google,

  # IP address geocoding service (see below for supported options):
  # :ip_lookup => :maxmind,

  # TODO: usar api en production
  # to use an API key:
  # :api_key => "AIzaSyAzDwNkcCrNeUQnkWkhBQus01tad3gk8eY",

  # geocoding service request timeout, in seconds (default 3):
  :timeout => 5,

  # set default units to kilometers:
  :units => :km,

  # caching (see below for details):
  # :cache => Redis.new,
  # :cache_prefix => "..."

)