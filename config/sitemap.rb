### Run on localhost ###
# rake sitemap:refresh:no_ping

# SitemapGenerator::Sitemap.default_host = "http://7d31e47b.ngrok.io"
SitemapGenerator::Sitemap.default_host = ActionMailer::Base.asset_host
SitemapGenerator::Sitemap.compress = false

SitemapGenerator::Sitemap.create do

  group(filename: :cat_sitemap, compress: false) do
    add root_path, priority: 1.0, changefreq: 'weekly'

    BannerCategory.all.each do |cat|
      add friendly_search_path(category: cat.slug), priority: 1.0, changefreq: 'weekly'
    end

    add friendly_search_path(category: 'todos'), priority: 1.0, changefreq: 'weekly'
  end


  State.all.each do |state|
    state_string = state.slug.gsub('-','_')
    group(filename: "ads_#{state_string}_sitemap", compress: false) do
      
      BannerCategory.all.each do |cat|

        states = []
        state.cities.each do |city|
          city_banners = city.banners.where(banner_category_id: cat.id)
          if city_banners.any? && city_banners.non_deleted.any?
            add friendly_search_path(category: cat.slug, state: state.slug), priority: 0.9, changefreq: 'weekly' unless states.include?(state.slug)
            states << state.slug
          end
        end # cities
        
        # Non Canonical
        state.cities.each do |city|
          city_banners = city.banners.where(banner_category_id: cat.id, state_id: city.state_id)
          if city_banners.any? && city_banners.non_deleted.any? && !city.canonical?
            add friendly_search_path(category: cat.slug, state: state.slug, city: city.slug), priority: 0.7, changefreq: 'weekly'
          end
        end # cities
      end # banner

      states = []
        state.cities.each do |city|
          city_banners = city.banners
          if city_banners.any? && city_banners.non_deleted.any?
            add friendly_search_path(category: 'todos', state: state.slug), priority: 0.9, changefreq: 'weekly' unless states.include?(state.slug)
            states << state.slug
          end
        end # cities
        
        # Non Canonical
        state.cities.each do |city|
          city_banners = city.banners.where(state_id: city.state_id)
          if city_banners.any? && city_banners.non_deleted.any? && !city.canonical?
            add friendly_search_path(category: 'todos', state: state.slug, city: city.slug), priority: 0.7, changefreq: 'weekly'
          end
        end # cities

    end # group
  end # states




  ### REFACTOR! USE EACH_SLICE WITH INDEX
  
  banners = Banner.unscoped.non_deleted.order('impressions_count DESC').includes(:category)
  
  range_1 = banners[0..49999]
  range_2 = banners[50000..99999]
  range_3 = banners[100000..149999]
  range_4 = banners[150000..199999]

  group(filename: :impressions_sitemap, compress: false) do
    range_1.each{|post| add post_with_category_path(post), priority: 0.5, changefreq: 'daily' }
  end

  group(filename: "impressions_sitemap-2", compress: false) do
    range_2.each{|post| add post_with_category_path(post), priority: 0.5, changefreq: 'daily' } if range_2
  end

  group(filename: "impressions_sitemap-3", compress: false) do
    range_3.each{|post| add post_with_category_path(post), priority: 0.5, changefreq: 'daily' } if range_3
  end

  group(filename: "impressions_sitemap-4", compress: false) do
    range_4.each{|post| add post_with_category_path(post), priority: 0.5, changefreq: 'daily' } if range_4
  end

end
