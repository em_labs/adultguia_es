every 1.day, :at => '3:00 am' do
  rake "-s sitemap:refresh"
end

every 6.hours do
  rake "app:auto_top_old_posts"
end
