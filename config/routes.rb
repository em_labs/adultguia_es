class ActionDispatch::Routing::Mapper
  def draw(routes_name)
    instance_eval(File.read(Rails.root.join("config/routes/#{routes_name}.rb")))
  end
end

Rails.application.routes.draw do

  # tanto WWW como SIN subdomain, hacen redirect
  constraints(subdomain: ['www','']) do
    scope as: 'www' do
      root to: 'country_selector#show'
      get '*path' => 'country_selector#show'
    end
  end

  draw :es_redirections
  draw :mx_redirections
  
  # el app solo acepta routes bajo el subdomain de su pais
  constraints(subdomain: Rails.configuration.x.app_country) do

    devise_for :super_users, controllers: {
      sessions: 'super/users/sessions',
      passwords: 'super/users/passwords',
    }

    ### SUPER
    namespace :super do
      resources :banners do
        get :toggle_recommended, on: :member
        post :delete_multiple_banners, on: :collection 
      end

      resources :payments
      resources :users
      resources :banner_categories
      resources :payment_types
      resources :pack_discounts
      resources :super_users
      resources :app_settings
      resources :html_texts
      resources :image_banners
      resources :external_banners

      get 'reportes'                       => 'reports#index',              as: 'reports'
      get 'reportes/titulos_duplicados'    => 'reports#duplicated_title',   as: 'duplicated_title_reports'
      get 'reportes/contenidos_duplicados' => 'reports#duplicated_content', as: 'duplicated_content_reports'
      get 'reportes/titulos_similares'     => 'reports#similar_title',      as: 'similar_title_reports'
      get 'reportes/contenidos_similares'  => 'reports#similar_content',    as: 'similar_content_reports'
      get 'reportes/usuarios'              => 'reports#users',              as: 'users_reports'
      get 'reportes/usuarios_semanal'      => 'reports#weekly_users',       as: 'weekly_users_reports'

      root to: 'users#index'

      match 'similar_text' => 'banners#similar_text', as: :similar_text, via: [:get, :post]
    end

    ### FRONTEND

    devise_for :users, controllers: {
      sessions: 'users/sessions',
      passwords: 'users/passwords',
      registrations: 'users/registrations',
      confirmations: 'users/confirmations',
      # omniauth_callbacks: "users/omniauth_callbacks",
    }
    resources :users
    resources :banners

    get  'nuevo-anuncio-y-registro' => 'banners#anon_new_banner', as: 'anon_new_banner'
    post 'anon_create_banner' => 'banners#anon_create_banner', as: 'anon_create_banner'
    get '/users/sign_up', to: redirect('nuevo-anuncio-y-registro', status: 301)

    resources :cities do
      collection do
        get :search
      end
    end

    get '/anuncios-eroticos/escorts-y-putas/distrito-federal', to: redirect('/anuncios-eroticos/escorts-y-putas/distrito-federal-df', status: 301)

    # search url pretty para SEO
    # uso el keyword "putas" para crear variaciones de patrones segun los params pasados
    get '/nuevo-anuncio' => 'banners#new', as: "new_post"
    get '/anuncios-eroticos/:category/:state(/contactos/:term)' => "posts#index", as: "search_state_term"
    get '/anuncios-eroticos/:category(/contactos/:term)'        => "posts#index", as: "search_term"
    get '/anuncios-eroticos/:category(/:state)(/:city)(/:term)' => "posts#index", as: "friendly_search"
    get '/anuncios-eroticos/:category(/:state)(/:city)(/:term)' => "posts#index", as: "full_search"
    get '/anuncios-eroticos/:category' => "posts#index", as: "only_category"


    get '/anuncio-erotico/:id' => "posts#show", as: "post_with_category"
    get '/posts/:id/get_phone' => "posts#show_phone", as: 'post_phone'

    resources :posts, path: 'anuncios' do
      get :search, on: :collection
    end
    get '/anuncios-eroticos/:category_id/:state_id'          => 'posts#search_single_state', as: :search_single_state
    get '/anuncios-eroticos/:category_id/:state_id/:city_id' => 'posts#search_single_city',  as: :search_single_city

    resources :banner_contact_requests
    resources :user_favorites do
      post :toggle, on: :member
    end

    resources :sent_favorites
    resources :feedback_requests

    # pagos
    get '/historial-pagos' => 'payments#index', as: 'my_payments'
    get '/historial-pagos/:id' => 'payments#show', as: 'view_payment'
    get '/payments/est_price' => 'payments#get_price', as: 'est_price'
    resources :payments

    # paypal checkout
    get '/paypal/checkout' => 'paypal#checkout', as: 'paypal_checkout'
    get '/paypal/success' => 'paypal#success', as: 'paypal_success'
    get '/paypal/fail' => 'paypal#fail', as: 'paypal_fail'

    # match "/delayed_job" => DelayedJobWeb, :anchor => false, via: [:get, :post]

    get '/mejora-adultguia' => 'feedback_requests#new', as: "feedback"
    get '/aviso-legal' => 'statics#aviso_legal', as: "aviso_legal"
    get '/politica-privacidad-cookies' => 'statics#politica_privacidad', as: "politica_privacidad"

    get 'feed' => 'posts#feed'

    root to: 'posts#index'

    get '/remote-cities' => 'cities#search', as: "remote_cities"

    post 'set_paid_location' => 'banners#set_paid_location', as: 'set_paid_location'

    get 'validar_anuncio' => 'banners#validate', as: 'validate_banner'
    get 'resend_email_validation' => 'banners#resend_email_validation', as: 'resend_email_validation'

  end

end
