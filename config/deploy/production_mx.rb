server 'mx.adultguia.com', roles: %w{app db web}

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/home/mx/app'
set :tmp_dir, "/home/mx/tmp"

set :ssh_options, {
 # keys: %w(/home/rlisowski/.ssh/id_rsa),
 forward_agent: false,
 auth_methods: %w(password),
 password: "XFYRNpp4DTz7",
 user: 'mx'
}