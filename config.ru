# This file is used by Rack-based servers to start the application.

require ::File.expand_path('../config/environment', __FILE__)

# use Rack::CanonicalHost do |env|
#   case Rails.env.to_sym
#     when :production_mx then 'mx.adultguia.com'
#     when :production then 'adultguia.com'
#   end
# end

run Rails.application

if Rails.env.production?
  DelayedJobWeb.use Rack::Auth::Basic do |username, password|
    username == 'vjnunez' && password == 'ZFUrD3X2UHLd'
  end
end