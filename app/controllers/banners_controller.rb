class BannersController < BaseController

  before_action :authenticate_user!, except: [:anon_new_banner, :anon_new_user, :anon_create_banner, :anon_create_user]
  before_action :set_banner, only: [:show, :edit, :update, :destroy]

  def index
    @banners = Banner.unscoped.non_deleted.where(user: current_user).includes(:phone_views).page(params[:page]).per(10)
  end

  def show
  end

  def new
    @banner = current_user.banners.new
    # inherit user location
    @banner.email = current_user.email
    @banner.state = current_user.state if current_user.state.present?
    @banner.city = current_user.city if current_user.city.present?
  end

  def edit
  end

  def create
    @banner = current_user.banners.new(banner_params)
    @banner.validated = false

    if @banner.save
      BannerMailer.validate(@banner).deliver_now
      redirect_to post_permalink(@banner), notice: '¡Anuncio creado con éxito! (Recibiste un email para validar tu anuncio)'
    else
      render :new
    end
  end

  def update
    if @banner.update(banner_params)
      redirect_to edit_banner_path(@banner), notice: 'Cambios guardados'
    else
      render :edit
    end
  end

  def destroy
    @banner.destroy
    redirect_to banners_url, notice: 'Anuncio eliminado'
  end

  def set_paid_location
    paid_banners = current_user.paid_banner_locations.where(banner_id: params[:paid_banner_location][:banner_id])
    
    if paid_banners.any?
      paid_banner = paid_banners.last
      paid_banner.update_attributes(
        banner_id: params[:paid_banner_location][:banner_id],
        state_id:  params[:paid_banner_location][:state_id],
        city_id:   params[:paid_banner_location][:city_id],
        banner_category_id: params[:paid_banner_location][:banner_category_id],
      )

      notice_message = "Ubicación actualizada correctamente"
      redirect_to :back, notice: notice_message
    else
      paid_banner_location = PaidBannerLocation.new(
        user_id:   current_user.id,
        banner_id: params[:paid_banner_location][:banner_id],
        state_id:  params[:paid_banner_location][:state_id],
        city_id:   params[:paid_banner_location][:city_id],
        banner_category_id: params[:paid_banner_location][:banner_category_id]
      )

      if paid_banner_location.save
        notice_message = "Ubicación guardada correctamente"
        redirect_to :back, notice: notice_message
      else
        notice_message = "Ocurrió un error guardando la ubicación: #{paid_banner_location.errors.full_messages.join(', ')}"
        redirect_to :back, alert: notice_message
      end
    end
  end

  # Anonymous
  def anon_new_banner    
    case Rails.configuration.x.app_country
    when 'mx'
      then
      city = City.where(slug: 'benito-juarez', state_slug: 'distrito-federal-df').last
    when 'es'
      then
      city = City.where(slug: 'madrid', state_slug: 'madrid').last
    else
      city = City.where(slug: 'madrid', state_slug: 'madrid').last
    end

    @banner = Banner.new
  end

  def anon_create_banner
    @user = User.find_for_authentication(email: params[:user_email])
    
    if @user.present? && @user.valid_password?(params[:user_password])
      @banner = @user.banners.new(banner_params)

      @banner.validated = false
      if @banner.save
        BannerMailer.validate(@banner).deliver_now
        sign_in(@user, scope: :user) and redirect_to post_with_category_path(id: @banner.id), notice: '¡Anuncio creado con éxito! (Recibiste un email para validar tu anuncio)'
      else
        render :anon_new_banner
      end
    else
      first_name = params[:user_email].split("@").first
      last_name  = first_name

      @user = User.new(
        first_name: first_name,
        last_name:  last_name,
        state_id:   banner_params[:state_id],
        city_id:    banner_params[:city_id],
        email:      params[:user_email],
        password:   params[:user_password],
        password_confirmation: params[:user_password],
        confirmed_at: Time.now.utc
      )
      
      @banner = @user.banners.new(banner_params)
      @banner.validated = false

      if @user.save
        if @banner.save
          BannerMailer.validate(@banner).deliver_now

          #sign_in(@user, scope: :user) and redirect_to post_with_category_path(id: @banner.id), notice: '¡Anuncio creado con éxito!'
          sign_in(@user, scope: :user) and redirect_to only_category_path(category: @banner.category.slug), notice: '¡Anuncio creado con éxito! (Recibiste un email para validar tu anuncio)'
        else
          render :anon_new_banner
        end
      else
        render :anon_new_banner
      end

    end
  end

  def validate
    banner = Banner.unscoped.find_by(validation_uuid: params[:uuid])
    if banner.present?
      banner.update_attribute(:validated, true)
      banner.save
      redirect_to post_with_category_path(id: banner.id), notice: '¡Anuncio validado con éxito!'
    else
      redirect_to root_path
    end
  end

  def resend_email_validation
    banner = Banner.unscoped.where(user: current_user).friendly.find(params[:id])
    if banner.present?
      BannerMailer.validate(banner).deliver_now
      redirect_to :back, notice: 'Email de validación enviado con éxito!'
    else
      redirect_to root_path
    end
  end

  private
    def set_banner
      @banner = Banner.unscoped.non_deleted.where(user: current_user).friendly.find(params[:id])
    end

    def banner_params
      params.require(:banner).permit(:banner_category_id, :title, :visible_from, :email, :phone, :description, :state_id, :city_id, :price, :alerts, images_attributes: [:_destroy, :id, :file, :file_cache, :position, :title, :alt])
    end
end
