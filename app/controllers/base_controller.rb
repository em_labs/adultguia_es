class BaseController < ApplicationController

  def current_payment
    @current_payment ||= current_user.payments.find_by(id: session[:payment_id])
  end
  helper_method :current_payment

  def user_favs
    @user_favs ||= (current_user ? current_user.favorites.pluck(:banner_id) : nil)
  end
  helper_method :user_favs

end