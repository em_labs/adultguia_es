class CitiesController < BaseController

  def index
    @cities = []
    if params[:state]
      @cities = City.where(state_id: params[:state].to_i)
    end
    render json: @cities.to_json
  end

  def search
    cities_cache = Rails.cache.read('cities_cache')

    city_results  = cities_cache.where("name LIKE ?", "%#{params[:q]}%")
    state_results = cities_cache.where("state_name LIKE ?", "%#{params[:q]}%")
    
    cities_collection = city_results.collect{  |city| { id: "city-#{city.id}", text: "#{city.state_name} - #{city.name}" } }.uniq
    states_collection = state_results.collect{ |city| { id: "state-#{city.state_id}", text: city.state_name } }.uniq

    render json: (states_collection + cities_collection).to_json
  end

  #def search1
  #  cities_cache = Rails.cache.read('static_cities_cache')
  #  states_cache = Rails.cache.read('static_states_cache')

  #  state_results = states_cache.select{|id, name, slug| slug.include?(params[:q].parameterize)}
  #  city_results  = cities_cache.select{|id, name, slug| slug.include?(params[:q].parameterize)}

  #  cities_collection = city_results.collect{|city| {id: city[0],  text: city[1]} }
  #  states_collection = state_results.collect{|state| {id: state[0], text: state[1]} }

  #  render json: (states_collection + cities_collection).to_json
  #end

end
