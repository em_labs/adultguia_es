class PaypalController < BaseController

  before_action :authenticate_user!

  def checkout
    paypal_options = {
      no_shipping: true, # if you want to disable shipping information
      # allow_note: false, # if you want to disable notes
      pay_on_paypal: true # if you don't plan on showing your own confirmation step
    }
    response = api_request.setup(
      payment_request,
      paypal_success_url(banner: current_payment.banner_id),
      paypal_fail_url(banner: current_payment.banner_id),
      paypal_options
    )
    redirect_to response.redirect_uri
  end

  def success
    token = params[:token]
    payer_id = params[:PayerID]

    response = api_request.checkout!(
      token,
      payer_id,
      payment_request
    )
    # inspect this attribute for more details
    # logger.ap response.payment_info
    current_payment.process(response.payment_info.first)

    banner = Banner.find(current_payment.banner_id)
    if current_payment.complete?
      # OK
      session[:payment_id] = nil # reset cart

      PaidBannerLocation.create(
        user_id:   current_user.id,
        banner_id: banner.id,
        state_id:  banner.state_id,
        city_id:   banner.city_id,
        banner_category_id: banner.banner_category_id
      )

      redirect_to post_path(banner), notice: "¡El pago se ha realizado con éxito!" 
    else
      redirect_to post_path(banner), alert: "No se ha podido completar la transacción, inténtalo de nuevo o contáctanos."
    end
  end

  def fail
    banner = Banner.find(params[:banner])
    redirect_to post_path(banner), alert: "No se ha podido completar la transacción, inténtalo de nuevo o contáctanos."
  end

  private

  def payment_request
    @payment_request ||= Paypal::Payment::Request.new(
      :currency_code => :EUR,   # if nil, PayPal use USD as default
      :description   => "#{current_payment.payment_type.title}",    # item description
      :quantity      => 1,      # item quantity
      :amount        => current_payment.total,   # item value
    )
  end

  def api_request
    Paypal.sandbox! if Rails.env.development?
    
    @api_request ||= Paypal::Express::Request.new(
      :username   => AppSetting.get('paypal_username'),
      :password   => AppSetting.get('paypal_password'),
      :signature  => AppSetting.get('paypal_signature')
    )

    #@api_request ||= Paypal::Express::Request.new(
    #  :username   => "marcomontes_api1.gmail.com",
    #  :password   => "RV245BWH8MBPVSJH",
    #  :signature  => "AFcWxV21C7fd0v3bYYYRCpSSRl31AcPYeUpuSdfqz3dxenCEKmAVjgWt"
    #)
  end

end
