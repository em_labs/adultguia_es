class PostsController < BaseController

  before_filter :check_url_redirection

  def feed
    rel = Banner.unscoped.is_valid.non_deleted.visible
    if params[:category]
      cat = BannerCategory.find_by(slug: params[:category])
      rel = rel.where(category: cat) if cat
    end
    @banners = rel.order('created_at desc').limit(50)
    respond_to do |format|
      format.rss { render :layout => false }
    end
  end

  # formatea la url pretty para SEO
  def search
    if !params[:q] || (params[:q][:term].blank? && params[:q][:city_id].blank? && params[:q][:category].blank?)
      redirect_to root_path and return
    end

    url_params = SearchAdapter.search(params[:q])

    redirect_to search_state_term_url(url_params) and return if url_params[:state] && !url_params[:city]
    redirect_to search_term_url(url_params)       and return if !url_params[:state] && !url_params[:city]
    redirect_to friendly_search_url(url_params)
  end

  def search_single_state
    states_cache = Rails.cache.read('states_cache')
    state        = states_cache.find_by(slug: params[:state_id])
    category     = params[:category].present? ? params[:category] : 'todos'
    opts         = {}
    opts[:state] = state.slug
    url_params   = { category: category }.merge(opts)
    session[:single_state] = true

    redirect_to search_state_term_url(url_params)
  end

  def search_single_city
    cities_cache = Rails.cache.read('cities_cache')

    state        = cities_cache.find_by(state_slug: params[:state_id])
    city         = cities_cache.find_by(slug: params[:city_id])
    category     = params[:category].present? ? params[:category] : 'todos'
    opts         = {}
    opts[:state] = city.state_slug
    opts[:city]  = city.slug
    url_params   = { category: category }.merge(opts)
    session[:single_state] = true
    session[:single_city]  = true

    redirect_to friendly_search_url(url_params)
  end

  def index
    # populate "q" hash for search form persistence
    @term = params[:term]
    @category = params[:category].parameterize if params[:category]

    search_params = {}
    search_params[:term] = @term.titleize if @term

    city  = State.find_city(params[:state], params[:city])
    state = State.find_state(params[:state])

    search_params[:city_id]  = city.id if city
    search_params[:state_id] = state.id if state

    @location = ""
    @location = "#{city.name}, " if city
    @location << "#{state.name}" if state

    @bread_city = city
    @bread_state = state


    if params[:state] && !@bread_state
      redirect_to friendly_search_url(category: @category), status: 301

    elsif params[:city].present? && !@bread_city && @bread_state
      redirect_to friendly_search_url(category: @category, state: @bread_state.slug), status: 301

    elsif @category && @bread_state && @bread_city && @bread_city.state != @bread_state
      redirect_to friendly_search_url(category: @category, state: @bread_state.slug), status: 301

    else
      if @category
        category = BannerCategory.find_by(slug: @category)
        search_params[:category] = category.id if category
      else
        @category = 'todos'
      end

      # canonical al home para el path de /anuncios o /anuncios?page=2
      @canonical_url = "#{ActionMailer::Base.asset_host}" if search_params.empty?

      @q = OpenStruct.new(search_params)
      @banners = search_scope({is_top: false}).page(params[:page]).per(50)

      # hago un group por date, asi lo muestro por renglones en el listing
      @grouped_banners = @banners.group_by{|b| b.sort_date.to_date }

      if category && (category.class.name == "BannerCategory")
        @state_html_texts = HtmlText.where(banner_category_id: category.id, state_id: state.id, city_id: nil) if state && !params[:page]
        @city_html_texts  = HtmlText.where(banner_category_id: category.id, city_id: city.id) if city && !params[:page]

        @state_image_banners = ImageBanner.where(banner_category_id: category.id, state_id: state.id, city_id: nil) if state && !params[:page]
        @city_image_banners  = ImageBanner.where(banner_category_id: category.id, city_id: city.id) if category && city && !params[:page]
        @all_location_banners = ImageBanner.where(banner_category_id: category.id, all_locations: true) unless (@state_image_banners && @state_image_banners.any?) || (@city_image_banners && @city_image_banners.any?)

        
        if state && !city
          paid_banner_locations = PaidBannerLocation.where(banner_category_id: category.id, state_id: state.id) 
          
          canonicals = []
          paid_banner_locations.each{|pbl| canonicals << pbl if pbl.city.slug == pbl.city.state_slug }

          if canonicals.any?
            paid_banner_locations_ids = canonicals.collect(&:banner_id)
          else
            paid_banner_locations_ids = []
          end

        elsif state && city
          paid_banner_locations = PaidBannerLocation.where(banner_category_id: category.id, state_id: state.id, city_id: city.id) 
          paid_banner_locations_ids = paid_banner_locations.collect(&:banner_id)

        else
          paid_banner_locations_ids = []
        end

        # top anuncios
        @top_banners = paid_search_scope({id: paid_banner_locations_ids, is_top: true}, 'rand()').limit(10)
      end

      # For Sidebar
      states_with_posts        = State.options_with_banners(@category)
      states_with_posts_size   = states_with_posts.size
      states_with_posts_middle = (states_with_posts_size+1)/2
      @states_with_posts_1     = states_with_posts[0..states_with_posts_middle]
      @states_with_posts_2     = states_with_posts[(states_with_posts_middle+1)..states_with_posts_size]

      if session[:single_state] || @bread_state
        @single_state = true
        session[:single_state] = nil

        cities_with_posts        = @bread_state.cities_with_banners(@category)
        cities_with_posts_size   = cities_with_posts.size
        cities_with_posts_middle = (cities_with_posts_size + 1)/2
        @cities_with_posts_1     = cities_with_posts[0..cities_with_posts_middle]
        @cities_with_posts_2     = cities_with_posts[(cities_with_posts_middle+1)..cities_with_posts_size]
      end

      @current_url = request.original_url
    end
  end

  def show
    @q = OpenStruct.new(params[:q])

    banner = Banner.unscoped.friendly.find(params[:id])

    if banner.user == current_user
      @banner = banner
      get_banner_vars
    elsif !banner.validated || banner.deleted?
      url_params = { category: banner.category, state: banner.state.slug, city: banner.city.slug }
      redirect_to friendly_search_url(url_params), status: :moved_permanently
    else
      @banner = banner
      get_banner_vars
    end
  end

  def show_phone
    @banner = Banner.friendly.find(params[:id])
    @banner.phone_views.create(session_hash: session.id, ip_address: request.remote_ip)
    # BannerMailer.new_phone_view(@banner).deliver_later if @banner.alerts?
    render nothing: true unless request.xhr?
  end

  private

  # scope segun los params del search form
  def search_scope(conditions = {}, order = 'sort_date desc, id desc')
    rel = Banner.unscoped.is_valid.non_deleted.visible.includes(:images, :category, :city, :state).where(conditions)

    rel = rel.where(banner_category_id: @q.category) if @q.category.present?
    rel = rel.where(state_id: @q.state_id) if @q.state_id.present?
    rel = rel.where(city_id: @q.city_id) if @q.city_id.present?

    rel = PostFilter.new.filter(rel, @q.term.to_s)
    rel = rel.order(order)
    rel
  end

  def paid_search_scope(conditions = {}, order = 'sort_date desc, id desc')
    rel = Banner.unscoped.is_valid.non_deleted.visible.includes(:images, :category, :city, :state).where(conditions)

    rel = PostFilter.new.filter(rel, @q.term.to_s)
    rel = rel.order(order)
    rel
  end

  def check_url_redirection
    if params[:category] == "Escorts y Putas" || params[:category] == "Masajes eróticos"
      redirect_to friendly_search_url(category: params[:category].parameterize), status: 301
    end
  end

  def get_banner_vars
    impressionist(@banner)
    @contact_request = @banner.contact_requests.new

    # payments
    @payment_types = PaymentType.non_publish.includes(:discounts).all
    @active_payments = @banner.payments.active.all

    @bread_city = @banner.city
    @bread_state = @banner.state

    # related posts
    @related_posts = Banner.where(category: @banner.category, city: @bread_city).where.not(id: @banner.id).order('rand()').limit(5)

    @paid_banner_location = @banner.paid_banner_location || PaidBannerLocation.new
  end

end
