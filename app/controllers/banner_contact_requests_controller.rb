class BannerContactRequestsController < BaseController

  def create
    @banner = Banner.find(s_params[:banner_id])
    @contact_request = @banner.contact_requests.new(s_params)
    # metadata
    @contact_request.session_hash = session.id
    @contact_request.ip_address = request.remote_ip
    
    if @contact_request.save
      flash.now[:contact_request] = "Tu mensaje ha sido enviado con éxito"
      @contact_request = @banner.contact_requests.new
    end
  end

  private

  def s_params
    params.require(:banner_contact_request).permit(:banner_id, :name, :email, :phone, :body)
  end

end