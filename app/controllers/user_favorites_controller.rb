class UserFavoritesController < BaseController

  before_action :authenticate_user!

  def index
    @favorites = current_user.favorites.page(params[:page]).per(10)
  end

  def toggle
    @banner = Banner.find(params[:id])
    faved = current_user.favorites.pluck(:banner_id).include?(@banner.id)
    if faved
      current_user.favorites.find_by(banner: @banner).destroy
    else
      current_user.favorites.create!(banner: @banner)
    end
    @banner.reload
    @user_favs = current_user.favorites.pluck(:banner_id)
  end

end