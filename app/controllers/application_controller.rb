class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception
  before_action :set_preferred_version
  before_action :set_raven_context

  force_ssl if: :ssl_configured?

  def ssl_configured?
    !Rails.env.development?
  end

  def post_permalink(banner)
    post_with_category_url(banner)
  end
  helper_method :post_permalink

  def set_preferred_version
    user_loc = get_user_location.downcase

    available_opts = {
      'rest_of_world'=>{
        title: 'AdultGuia España',
        url: 'https://adultguia.com'
      },
      'america'=>{
        title: 'AdultGuia México',
        url: 'https://mx.adultguia.com'
      },
    }
    current_app_region = available_opts[Rails.configuration.x.app_country] == 'mx' ? 'america' : 'rest_of_world'
    user_loc_region = user_loc.include?('america') ? 'america' : 'rest_of_world'
    
    if user_loc_region != current_app_region
      @suggested_website_version = available_opts[user_loc_region]
      @current_website_version = available_opts[current_app_region]
    end
  end

  private

  def get_user_location
    return session[:current_user_location] if session[:current_user_location]

    # ip = '146.83.255.255' # chile test
    ip = request.ip

    url = "http://freegeoip.net/json/#{ip}"
    uri = URI(url)
    response = Net::HTTP.get(uri)
    begin
      res = JSON.parse(response)
      session[:current_user_location] = res['time_zone']
      res['time_zone']
    rescue => e
      nil
    end
  end

  def set_raven_context
    Raven.user_context(id: session[:current_user_id]) # or anything else in session
    Raven.extra_context(params: params.to_unsafe_h, url: request.url)
  end

end
