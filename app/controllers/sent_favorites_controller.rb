class SentFavoritesController < BaseController

  before_action :authenticate_user!

  def new
    @sent_favorite = current_user.sent_favorites.new(banner_id: params[:banner])
  end

  def create
    @sent_favorite = current_user.sent_favorites.new(s_params)
    if @sent_favorite.save
      flash.now[:sent_favorite] = "¡Mensaje enviado!"
      # reset
      @sent_favorite = current_user.sent_favorites.new(banner_id: @sent_favorite.banner.id)
    end
    render :new
  end

  private

  def s_params
    params.require(:sent_favorite).permit(:banner_id, :email, :message)
  end

end