module Super

class PaymentTypesController < Super::BaseController
  
  def index
    @types = PaymentType.page(params[:page]).per(20)
  end

  def edit
    @type = PaymentType.find(params[:id])
  end

  def update
    @type = PaymentType.find(params[:id])
    if @type.update(s_params)
      redirect_to edit_super_payment_type_path(@type), notice: "Cambios guardados"
    else
      render :edit
    end
  end

  private

  def s_params
    params.require(:payment_type).permit(:title, :description, :price, :active, discounts_attributes: [:id, :_destroy, :days_from, :days_to, :discount])
  end

end

end