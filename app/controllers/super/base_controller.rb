class Super::BaseController < ApplicationController

  before_action :authenticate_super_user!

  layout 'super'

end