module Super

  class ReportsController < Super::BaseController
    
    def index
    end

    def duplicated_title
      banners = Banner.select(:title).group(:title).having("count(*) > 1")
      @banners = Banner.where(title: banners.collect(&:title))
    end

    def similar_title
      banners = Banner.last(300)
      repeated = []

      banners.each do |main_banner|
        banners.each do |banner|
          if is_similar?(main_banner.title, banner.title) && main_banner != banner
            repeated << main_banner if !repeated.include?(main_banner)
            repeated << banner if !repeated.include?(banner)
            repeated << "-"
          end
        end
      end
      
      @banners = repeated
    end

    def duplicated_content
      banners = Banner.select(:description).group(:description).having("count(*) > 1")
      @banners = Banner.where(description: banners.collect(&:description))
    end

    def similar_content
      banners = Banner.last(100)
      repeated = []

      banners.each do |main_banner|
        banners.each do |banner|
          if is_similar?(main_banner.description, banner.description) && main_banner != banner
            repeated << main_banner if !repeated.include?(main_banner)
            repeated << banner if !repeated.include?(banner)
            repeated << "-"
          end
        end
      end
      
      @banners = repeated
    end

    def users
      date  = Date.parse("sunday")
      delta = date < Date.today ? 0 : 7
      @week = date + delta
      
      weeks = []
      (1..30).each_with_index{ |w,i| weeks << [(@week.to_date - i.week).to_formatted_s(:long), i] }
      @weeks = weeks

      if params[:week].present?
        @filter = true
        @week = @week - params[:week].to_i.week
        @users = User.all.joins(:banners).where('banners.created_at between ? and ?', @week, @week + 1.week).uniq.page(params[:page]).per(200)
        @filter_msg = "de la semana: #{@week}"
      else
        @users = User.all.page(params[:page]).per(200)
        @filter_msg = 'de todos los tiempos'
      end
    end

    def weekly_users
      @users = []
    end

    def is_similar?(string_1, string_2, percent = 70)
      similar_percent = string_1.similar(string_2).to_i
      return (similar_percent >= percent) ? true : false
    end
  
  end

end
