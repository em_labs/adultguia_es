module Super

class BannerCategoriesController < Super::BaseController
  
  def index
    @categories = BannerCategory.page(params[:page]).per(20)
  end

  def new
    @category = BannerCategory.new
  end

  def create
    @category = BannerCategory.new(s_params)
    if @category.save
      redirect_to super_banner_categories_url, notice: "Categoría creada"
    else
      render :new
    end
  end

  def edit
    @category = BannerCategory.friendly.find(params[:id])
  end

  def update
    @category = BannerCategory.friendly.find(params[:id])
    if @category.update(s_params)
      redirect_to edit_super_banner_category_path(@category), notice: "Cambios guardados"
    else
      render :edit
    end
  end

  def destroy
    categ = BannerCategory.friendly.find(params[:id])
    categ.destroy
    redirect_to super_banner_categories_url, notice: 'Categoría eliminada'
  end

  private

  def s_params
    params.require(:banner_category).permit!
  end

end

end