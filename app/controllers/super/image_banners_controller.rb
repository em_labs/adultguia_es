module Super

  class ImageBannersController < Super::BaseController
    before_action :set_image_banner, only: [:show, :edit, :update, :destroy]

    def index
      @image_banners = ImageBanner.all
    end

    def new
      @image_banner = ImageBanner.new
    end

    def edit
    end

    def create
      @image_banner = ImageBanner.new(image_banner_params)
      if @image_banner.save
        msg = 'Banner Creado'
      else
        msg = @image_banner.errors.full_messages.join(', ')
      end

      redirect_to super_image_banners_url, notice: msg
    end

    def update
      @image_banner.update(image_banner_params)
      redirect_to super_image_banners_url, notice: 'Banner Actualizado'
    end

    def destroy
      @image_banner.destroy
      redirect_to super_image_banners_url, notice: 'Banner Eliminado'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_image_banner
        @image_banner = ImageBanner.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def image_banner_params
        params.require(:image_banner).permit(:banner_category_id, :city_id, :state_id, :desktop_image, :mobile_image,
          :desktop_link, :mobile_link, :all_locations, :start_date, :end_date, :active)
      end
  end
end
