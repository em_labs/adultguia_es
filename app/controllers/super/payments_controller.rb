module Super

class PaymentsController < Super::BaseController
  
  def index
    @q = Payment.complete.includes(:banner, :payment_type).ransack(params[:q])
    @q.sorts = 'created_at desc' if @q.sorts.empty?
    @payments = @q.result(distinct: true).page(params[:page]).per(20)
  end

  def show
    @payment = Payment.includes(:banner, :payment_type).find(params[:id])
    @existing_schedules = @payment.parse_custom_times if @payment.payment_type.code == "subir_pack_custom"
  end

end

end