module Super

  class AppSettingsController < Super::BaseController

    def index
      @settings = AppSetting.where(key: 'auto_subir_old_posts')
    end

    def update
      setting = AppSetting.find(params[:id])
      if setting.update(s_params)
        render json: setting.to_json
      else
      end
    end

    private

    def s_params
      params.require(:app_setting).permit!
    end

  end

end