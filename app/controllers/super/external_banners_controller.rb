module Super

  class ExternalBannersController < Super::BaseController
    before_action :set_banner, only: [:index, :edit, :update]

    def index
    end

    def edit
    end

    def update
      @banner.update(banner_params)
      redirect_to super_external_banners_path, notice: 'Banners Actualizados'
    end

    private
      def set_banner
        @banner = ExternalBanner.first
      end

      def banner_params
        params.require(:external_banner).permit(:es_mobile, :mx_mobile, :es_desktop, :mx_desktop)
      end
  end
end
