module Super

  class BannersController < Super::BaseController
    
    def index
      @q = Banner.ransack(params[:q])
      @q.sorts = 'created_at desc' if @q.sorts.empty?
      @banners = @q.result(distinct: true).page(params[:page]).per(20)
    end

    def edit
      @banner = Banner.friendly.find(params[:id])
    end

    def update
      @banner = Banner.friendly.find(params[:id])
      if @banner.update(s_params)
        redirect_to edit_super_banner_url(@banner), notice: "Cambios guardados"
      else
        render :edit
      end
    end

    def destroy
      banner = Banner.friendly.find(params[:id])
      banner.destroy
      redirect_to super_banners_url, notice: 'Anuncio eliminado'
    end

    def toggle_recommended
      banner = Banner.friendly.find(params[:id])
      banner.toggle :recommended
      banner.save
      
      notice_text = (banner.recommended == true) ? 'Anuncio marcado como recomendado' : 'Anuncio desmarcado como recomendado'
      redirect_to super_banners_url, notice: notice_text
    end

    def delete_multiple_banners
      params["delete_banner_id"].each do |banner_id|
        banner = Banner.find(banner_id)
        banner.destroy
      end

      redirect_to super_banners_url, notice: 'Anuncios eliminados'
    end

    def similar_text
      if request.post?
        #@similar_text = params[:text1].similar(params[:text2])
        @similar_text = Banner.compare_texts(params[:text1], params[:text2])
      end
    end

    private

    def s_params
      params.require(:banner).permit!
    end
    
  end

end