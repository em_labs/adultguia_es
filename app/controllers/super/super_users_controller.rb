module Super

class SuperUsersController < Super::BaseController
  
  def index
    @users = SuperUser.page(params[:page]).per(20)
  end

  def new
    @user = SuperUser.new
  end

  def create
    @user = SuperUser.new(s_params)
    if @user.save
      redirect_to super_super_users_url, notice: "Usuario creado"
    else
      render :new
    end
  end

  def edit
    @user = SuperUser.find(params[:id])
  end

  def update
    @user = SuperUser.find(params[:id])
    if @user.update(s_params)
      redirect_to edit_super_super_user_path(@user), notice: "Cambios guardados"
    else
      render :edit
    end
  end

  def destroy
    user = SuperUser.find(params[:id])
    user.destroy
    redirect_to super_super_users_url, notice: 'Usuario eliminado'
  end

  private

  def s_params
    params.require(:super_user).permit!
  end

end

end