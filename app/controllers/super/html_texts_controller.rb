module Super

  class HtmlTextsController < Super::BaseController
    before_action :set_html_text, only: [:show, :edit, :update, :destroy]

    def index
      @html_texts = HtmlText.all
    end

    def new
      @html_text = HtmlText.new
    end

    def edit
    end

    def create
      @html_text = HtmlText.new(html_text_params)
      if @html_text.save
        msg = 'Texto HTML Creado'
      else
        msg = @html_text.errors.full_messages.join(', ')
      end

      redirect_to super_html_texts_url, notice: msg
    end

    def update
      @html_text.update(html_text_params)
      redirect_to super_html_texts_url, notice: 'Texto HTML Actualizado'
    end

    def destroy
      @html_text.destroy
      redirect_to super_html_texts_url, notice: 'Texto HTML Eliminado'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_html_text
        @html_text = HtmlText.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def html_text_params
        params.require(:html_text).permit(:banner_category_id, :city_id, :state_id, :html_block, :active)
      end
  end
end
