module Super

class UsersController < Super::BaseController
  
  def index
    @q = User.ransack(params[:q])
    @q.sorts = 'created_at desc' if @q.sorts.empty?
    @users = @q.result(distinct: true).page(params[:page]).per(20)
  end

  def show
    @user = User.includes(:banners).find(params[:id])
  end

  def destroy
    user = User.find(params[:id])
    user.destroy
    redirect_to super_users_url, notice: 'Usuario eliminado'
  end
  
end

end