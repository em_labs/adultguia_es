class PaymentsController < BaseController

  before_action :authenticate_user!

  def index
    @payments = with_deleted_scope
                .order('id desc')
                .page(params[:page]).per(20)
  end

  def show
    @payment = with_deleted_scope.find(params[:id])
    @existing_schedules = @payment.parse_custom_times if @payment.payment_type.code == "subir_pack_custom"
  end
  
  def new
    # si hay que personalizar alguna opción, se hace aquí (render view)
    @type = PaymentType.find(params[:type])
    @banner = current_user.banners.find(params[:banner])

    days = params[:days] ? params[:days].to_i : 1
    @payment = @banner.payments.new(
      payment_type: @type,
      price: @type.price,
      total: @type.get_price(days: days),
      discount_percent: @type.get_discount_percent(days),
      num_days: days,
      aplicable_days_of_week: Payment::DAYS_OF_WEEK.values,
      start_date: I18n.l(1.day.from_now, format: '%d/%m/%Y')
    )

    # sino, hacemos redirect a checkout de paypal
    if ['subir', 'destacar', 'top'].include?(@type.code)
      @payment.save!
      session[:payment_id] = @payment.id

      redirect_to paypal_checkout_path
      return
    end

    # se necesita un form para personalizar mas las opciones?
    case @type.code
    when "subir_pack_custom"
      date = 1.day.from_now
      @payment.schedules.build(
        time_hour: date.strftime("%H"),
        time_min: date.strftime("%M"),
      )
      # dias programados existentes para este banner
      @existing_schedules = @banner.payments.active.where(payment_type_id: @type.id).map(&:parse_custom_times).flatten
    when "subir_pack_auto"

    end

  end

  def create
    @payment = current_user.payments.new(s_params)
    @type = @payment.payment_type
    @banner = @payment.banner

    @payment.price = @type.price
    @payment.total = @type.get_price(days: @payment.num_days)
    @payment.discount_percent = @type.get_discount_percent(@payment.num_days)

    if @payment.save
      # go to payment...
      session[:payment_id] = @payment.id
      redirect_to paypal_checkout_path
    else
      render :new
    end
  end

  def get_price
    type = PaymentType.find(params[:type])
    render json: {price: type.get_price(days: params[:days].to_i) }.to_json
  end

  private

  # unscope banners para acceder tambien a los soft-deleted
  def with_deleted_scope
    banners_ids = current_user.banners.with_deleted.pluck(:id)
    Payment
          .includes(:banner, :payment_type)
          .where(banner: banners_ids )
          .complete
  end

  def s_params
    params.require(:payment).permit(:banner_id, :payment_type_id, :num_days, :schedule_hour, :schedule_min, :start_date, :aplicable_days_of_week => [], :schedules_attributes => [:id, :time_hour, :time_min, :_destroy])
  end

end