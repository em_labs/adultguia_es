class CountrySelectorController < ApplicationController

  def show
    loc = get_user_location.downcase

    located_in_america = loc && loc.include?('america')
    
    # con WWW

    if located_in_america
      site = 'https://mx.adultguia.com'
      s = 302
    elsif request.subdomain.present? && request.subdomain == 'mx'
      site = 'https://mx.adultguia.com'
      s = 302
    elsif request.subdomain.present? && request.subdomain == 'es'
      site = 'https://es.adultguia.com'
      s = 301
    elsif request.subdomain.present? && request.subdomain == 'www' && located_in_america
      site = 'https://mx.adultguia.com'
      s = 302
    else
      # naked domain? => ES
      site = 'https://es.adultguia.com'
      s = 301
    end

    site << "#{request.fullpath}"
    redirect_to site, status: s
  end

end