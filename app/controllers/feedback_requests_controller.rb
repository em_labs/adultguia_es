class FeedbackRequestsController < BaseController

  def new
    @request = FeedbackRequest.new
  end

  def create
    @request = FeedbackRequest.new(s_params)
    if verify_recaptcha(model: @request, attribute: :captcha) && @request.save
      @request = FeedbackRequest.new
      flash.now[:feedback_form] = "Tu mensaje ha sido enviado, ¡Gracias!"
    end
  end

  private

  def s_params
    params.require(:feedback_request).permit(:email, :message, :accept_terms)
  end

end