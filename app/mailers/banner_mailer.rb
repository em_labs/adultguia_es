class BannerMailer < ApplicationMailer

  default :from => "AdultGuia <info@adultguia.com>", 'X-MC-Metadata'=>Proc.new{ {app: "adultguia"}.to_json }
  
  
  def new_contact_request(request)
    @request = request
    @banner = request.banner
    mail(:to => request.banner.email, :subject => "Nuevo mensaje en AdultGuia")
  end

  def new_phone_view(banner)
    @banner = banner
    mail(:to => banner.email, :subject => "Un usuario ha visto tu teléfono en AdultGuia")
  end

  def send_to_friend(request)
    @request = request
    mail(:to => request.email, :subject => "#{request.user.first_name} ha compartido un enlace contigo en AdultGuia")
  end

  def validate(banner)
    @banner = banner
    mail(:to => banner.email, :subject => "Valida tu Anuncio en AdultGuia")
  end

end