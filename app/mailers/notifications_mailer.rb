class NotificationsMailer < ApplicationMailer

  default :from => "AdultGuia <info@adultguia.com>", 'X-MC-Metadata'=>Proc.new{ {app: "adultguia"}.to_json }
  
  def new_feedback(request)
    @request = request
    mail(:to => "info@adultguia.com", :subject => "Nuevo feedback en AdultGuia")
  end

end