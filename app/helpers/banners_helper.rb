module BannersHelper

  def set_category(banner)
    if banner.paid_banner_location
      banner.paid_banner_location.banner_category_id
    else
      #banner.banner_category_id
      nil
    end
  end

  def set_state(banner)
    if banner.paid_banner_location
      banner.paid_banner_location.state_id
    else
      nil
    end
  end

  def set_city(banner)
    if banner.paid_banner_location
      banner.paid_banner_location.city_id
    else
      nil
    end
  end

end
