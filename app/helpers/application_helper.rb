module ApplicationHelper

  def image_url(source)
    abs_path = image_path(source)
    unless abs_path =~ /^http/
      abs_path = "#{request.protocol}#{request.host_with_port}#{abs_path}"
    end
   abs_path
  end

  def category_city_link(category, city)
    "#{category.title} #{city.name}".mb_chars.upcase
  end

  def check_canonical(state, city)
    (state == city) ? true : false 
  end

  def canonical_to_state(city, url)
    slug_size = city.slug.size
    url_size  = url.size - 2

    new_url = url[0..(url_size - slug_size)]
  end

  def canonical_of_search(url)
    url.split('/')[0..-2].join("/")
  end

  def show_money(value)
    money_symbol = case Rails.configuration.x.app_country
    when 'es'
      '€'
    when 'mx'
      'MXN'
    else
      '€'
    end
    number_to_currency(value, unit: money_symbol)
  end

  def money_symbol
    case Rails.configuration.x.app_country
    when 'es'
      '€'
    when 'mx'
      'MXN'
    else
      '€'
    end
  end

end
