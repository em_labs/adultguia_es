#encoding: UTF-8

xml.instruct! :xml, :version => "1.0"
xml.rss :version => "2.0" do
  xml.channel do
    xml.title "AdultGuia"
    xml.author "AdultGuia"
    xml.description "ANUNCIOS Eróticos en #{AppSetting.get('site_country').mb_chars.upcase} ☆ GRATIS ☆ Excitantes"
    xml.link "#{ActionMailer::Base.asset_host}"
    xml.language "en"

    for pp in @banners
      xml.item do
        if pp.title
          xml.title pp.title
        else
          xml.title ""
        end
        xml.author "AdultGuia"
        xml.pubDate pp.created_at.to_s(:rfc822)
        # xml.link "https://www.codingfish.com/blog/" + pp.id.to_s + "-" + pp.alias
        xml.link post_permalink(pp)
        xml.guid pp.id

        text = strip_tags(pp.description)

        # if you like, do something with your content text here e.g. insert image tags.
        # Optional. I'm doing this on my website.
        # if pp.images.any?
        #   image_url = pp.images.first.file.url(:large)
        #   # image_url = pp.image.url(:large)
        #   image_caption = ""
        #   image_align = ""
        #   image_tag = "
        #       <p><img src='" + image_url +  "' alt='" + image_caption + "' title='" + image_caption + "' align='" + image_align  + "' /></p>
        #     "
        #   text = text.sub('{image}', image_tag)
        # end
        xml.description "<p>" + text + "</p>"

      end
    end
  end
end