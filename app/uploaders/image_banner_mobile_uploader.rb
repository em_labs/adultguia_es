# encoding: utf-8
class ImageBannerMobileUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick
  include CarrierWave::ImageOptimizer

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :thumb do
    process :resize_to_fill => [120, 120]
    process :optimize
  end

  version :medium do
    process :resize_to_limit => [400, 300]
    process :optimize
  end

  version :large do
    process :resize_to_limit => [800, 600]
    process :optimize
  end

  def store_dimensions
    if file && model
      model.width, model.height = ::MiniMagick::Image.open(file.file)[:dimensions]
    end
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

end
