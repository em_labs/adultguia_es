class @PaymentOptions

  # parent element de las opciones de pago
  p: ".payment-options"

  constructor: ->
    that = this

    $('form.payment-form').parsley()

    callback = (el) ->
      days = $(el).val()
      type = $(el).closest('.opt').data('type')
      that.getPrice(el, days, type)
    $('.js-num-days').on 'keyup change', (e) -> callback(this)
      
  getPrice: (el, days, type) ->
    $.getJSON '/payments/est_price', {days: days, type: type}, (data) ->
      $(el).siblings('.total-amount').html( accounting.formatMoney(data.price, "€", 2, ".", ",") );