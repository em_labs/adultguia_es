$ ->
  initSummernote()

  $('[data-toggle=tooltip]').tooltip()

  $('.selectize').selectize()

  $(document).on 'focus', 'input.datepicker', (e) ->
    $(this).datepicker
      dateFormat: 'dd/mm/yy'
      minDate: 0

  # user location popup
  available_countries = ['es', 'mx']
  if (window.x_app_country != window.x_user_country && _.contains(available_countries, window.x_user_country) && !Cookies.get('country_selector_dismissed'))
    $('#country_selector_modal').modal()
  $('#country_selector_modal').on 'hide.bs.modal', () ->
    Cookies.set('country_selector_dismissed', '1')

  # cookie consent
  window.addEventListener "load", () ->
    window.cookieconsent.initialise
      "palette": {
        "popup": {
          "background": "#000"
        },
        "button": {
          "background": "#892232"
        }
      },
      "content": {
        "message": "Utilizamos cookies propias y de analítica para mejorar tu experiencia de usuario. Si continúas navegando, consideramos que aceptas su uso.",
        "dismiss": "Entendido",
        "link": "Más información aquí",
        "href": "/politica-privacidad-cookies"
      }

window.initSummernote = ->
  $('.summernote').each ->
    el = $(this)
    h = if $(this).data('height') then $(this).data('height') else null
    el.summernote
      height: h
      toolbar: [
        # [groupname, [button list]]
        ['style', ['bold', 'italic', 'underline']],
        ['para', ['ul', 'ol']]
      ]
      onPaste: (e) ->
        # prevent paste
        e.preventDefault()
      disableDragAndDrop: true

window.cleanCode = (editor) ->
  # $('.summernote').code( CleanPastedHTML($(editor).code()) )
  $(editor).code( CleanPastedHTML($(editor).code()) )

window.CleanPastedHTML = (input) ->
  # 1. remove line breaks / Mso classes
  stringStripper = /(\n|\r| class=(")?Mso[a-zA-Z]+(")?)/g
  output = input.replace(stringStripper, ' ')
  # 2. strip Word generated HTML comments
  commentSripper = new RegExp('<!--(.*?)-->','g')
  output = output.replace(commentSripper, '')
  tagStripper = new RegExp('<(/)*(meta|link|span|\\?xml:|st1:|o:|font)(.*?)>','gi')
  # 3. remove tags leave content if any
  output = output.replace(tagStripper, '')
  # 4. Remove everything in between and including tags '<style(.)style(.)>'
  badTags = ['style','script','applet','embed','noframes','noscript']
  for i in [0...badTags.length]
    tagStripper = new RegExp('<'+badTags[i]+'.*?'+badTags[i]+'(.*?)>', 'gi')
    output = output.replace(tagStripper, '')
  # 5. remove attributes ' style="..."'
  badAttributes = ['style', 'start']
  for i in [0...badAttributes.length]
    attributeStripper = new RegExp(' ' + badAttributes[i] + '="(.*?)"','gi')
    output = output.replace(attributeStripper, '')
  output
