class @PaymentForm

  payment_type: null
  form: 'form.payment-form'

  constructor: (type) ->
    that = this
    that.payment_type = type

    that.initCustomPack() if type.code == "subir_pack_custom"
    that.initAutoPack() if type.code == "subir_pack_auto"
    $(that.form).parsley()

  initAutoPack: ->
    that = this
    $(that.form).find('input[id$=num_days]').on 'keyup change', (e) ->
      that.getPrice( $(this).val(), '.payment-total span')
    .change()

  initCustomPack: ->
    that = this

    $(document).on 'focus', '.start-date-picker', (e) ->
      $(this).datepicker
        dateFormat: 'dd/mm/yy'
        minDate: 1
        
    $('input#payment_num_days').on 'change', (e) ->
      days = parseInt($(this).val())
      that.getPrice(days, '.payment-total span')
    .change()

  getPrice: (days, elem_to_update) ->
    that = this
    params = { days: days, type: that.payment_type.id }
    $.getJSON '/payments/est_price', params, (data) ->
      $(elem_to_update).html( accounting.formatMoney(data.price, "€", 2, ".", ",") );