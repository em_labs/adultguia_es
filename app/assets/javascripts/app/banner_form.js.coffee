class @BannerForm

  form: 'form.js-banner-form'

  constructor: (banner) ->
    # @initImages()
    that = this

    location = new LocationSelects('#banner-form', '.selectize-states', '.selectize-cities')

    $(document).on 'click', @form+' .banner-images .move-up', (e) ->
      e.preventDefault()
      el = $(this).closest('.nested-fields')
      prev = $(el).prev('.nested-fields')
      $(el).insertBefore(prev)
      that.refreshPositions()

    $(document).on 'click', @form+' .banner-images .move-down', (e) ->
      e.preventDefault()
      el = $(this).closest('.nested-fields')
      next = $(el).next('.nested-fields')
      $(el).insertAfter(next)
      that.refreshPositions()

  refreshPositions: ->
    $('.banner-images .nested-fields').each (i, el) ->
      $(this).find('input[id$=position]').val(i+1)

  # initImages: ->
  #   that = this

  #   $('.banner-images .wrapper').sortable
  #     handle: ".handle"
  #     update: (e, ui) ->
  #       that.refreshPositions()