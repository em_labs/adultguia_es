class @LocationSelects

  constructor: (form, state_select, city_select) ->

    render_item = (data, escape) ->
      title = data.text
      title = data.name if title == undefined # ciudades
      title = "#{title.substring(0, 15)}..." if title.length > 15
      return '<div class="item">'+ escape(title) + '</div>'
    
    cities_select = $("#{form} #{city_select}").selectize
      maxItems: 1
      valueField: 'id'
      labelField: 'name'
      searchField: 'name'
      render: {
        item: render_item
      }

    $("#{form} #{state_select}").selectize
      maxItems: 1
      onChange: (value) ->
        $.ajax
          url: '/cities/',
          data: { state: value }
          type: 'GET'
          dataType: 'json'
          success: (res) ->
            api = cities_select[0].selectize
            api.clearOptions()
            api.addOption(res)
            api.open() if value != ""
      render: {
        item: render_item
      }