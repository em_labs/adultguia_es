$ ->
  initSummernote()

  $('.selectize').selectize()
  
  $(document).on 'focus', 'input.datepicker', (e) ->
    $(this).datepicker
      dateFormat: 'dd/mm/yy'

window.initSummernote = ->
  $('.summernote').each ->
    el = $(this)
    h = if $(this).data('height') then $(this).data('height') else null 
    el.summernote
      height: h
      toolbar: [
        # [groupname, [button list]]
        ['style', ['bold', 'italic', 'underline']],
        ['para', ['ul', 'ol']]
      ]
      onPaste: (e) ->
        # prevent paste
        e.preventDefault()
      disableDragAndDrop: true