//= require jquery
//= require jquery_ujs
//= require cocoon
// BOWER
//= require js-cookie/src/js.cookie.js
//= require bootstrap/dist/js/bootstrap
//= require jquery-ui/ui/core
//= require jquery-ui/ui/widget
//= require jquery-ui/ui/mouse
//= require jquery-ui/ui/sortable
//= require jquery-ui/ui/datepicker
//= require jquery-ui/ui/i18n/datepicker-es
//= require summernote/dist/summernote
//= require underscore/underscore
//= require parsleyjs/dist/parsley
//= require parsleyjs/src/i18n/es
//= require accountingjs/accounting.min
//= require masonry/dist/masonry.pkgd.min
//= require imagesloaded/imagesloaded.pkgd.min
//= require selectize/dist/js/standalone/selectize.min
//= require cookieconsent/build/cookieconsent.min.js
// APP
//= require_tree ./app