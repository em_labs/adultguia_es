//= require jquery
//= require jquery_ujs
//= require cocoon
// BOWER
//= require bootstrap/dist/js/bootstrap
//= require jquery-ui/ui/core
//= require jquery-ui/ui/widget
//= require jquery-ui/ui/mouse
//= require jquery-ui/ui/sortable
//= require jquery-ui/ui/datepicker
//= require summernote/dist/summernote
//= require selectize/dist/js/standalone/selectize.min
//= require_tree ./super
