class PostFilter
  include Minidusen::Filter

  filter :text do |scope, phrases|
    columns = [:title, :description]
    scope.where_like(columns => phrases)
  end

end