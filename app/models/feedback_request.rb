class FeedbackRequest < ActiveRecord::Base

  attr_accessor :accept_terms

  validates :message, presence: true
  validates :accept_terms, acceptance: true

  after_create :notify

  def notify
    NotificationsMailer.new_feedback(self).deliver_later
  end

end