class SearchAdapter

  def self.search(options)
    query = OpenStruct.new(options)

    # Category
    if query.category.present?
      category = BannerCategory.find(query.category).slug
    else
      category = 'todos'
    end

    # Search Term
    search_term = query.term.parameterize

    # State, City
    if query.city_id.present?
      if query.city_id.include?("city")
        city_id      = query.city_id.split("city-")[1].to_i
        cities_cache = Rails.cache.read('cities_cache')
        city         = cities_cache.find(city_id)
      end

      if query.city_id.include?("state")
        state_id     = query.city_id.split("state-")[1].to_i
        states_cache = Rails.cache.read('states_cache')
        state        = states_cache.find(state_id)
      end
    end

    if city
      city_param  = city.slug
      state_param = city.state.slug
    elsif state
      state_param = state.slug
    end

    { term: search_term, category: category, city: city_param, state: state_param }
  end

  def self.states
    case Rails.configuration.x.app_country
    when 'mx'
      self.mx_states
    else
      self.es_states
    end
  end

  def self.cities
    case Rails.configuration.x.app_country
    when 'mx'
      self.mx_cities
    else
      self.es_cities
    end
  end

  def self.es_states
  end

  def self.es_cities
  end

  def self.mx_states
  end

  def self.mx_cities
  end

end

