class UserFavorite < ActiveRecord::Base
  belongs_to :user
  belongs_to :banner, counter_cache: "favorites_count"

  validates :user, presence: true
  validates :banner_id, uniqueness: {scope: :user}

end