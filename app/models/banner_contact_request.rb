class BannerContactRequest < ActiveRecord::Base
  belongs_to :banner

  validates :banner, :body, presence: true
  validates :email, presence: true, email: true

  after_create :send_notification

  def send_notification
    BannerMailer.new_contact_request(self).deliver_later
  end

end