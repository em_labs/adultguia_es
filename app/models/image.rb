class Image < ActiveRecord::Base
  mount_uploader :file, ImageUploader

  belongs_to :banner

  validates :file, presence: true

  default_scope -> { order('position asc') }
end
