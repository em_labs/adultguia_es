class State < ActiveRecord::Base
  validates :name, uniqueness: true

  has_many :cities, dependent: :destroy
  has_many :banners, through:  :cities
  has_many :paid_banner_locations

  before_save :set_slug

  def set_slug
    self.slug = name.parameterize
  end

  def has_banners?
    banners = false
    cities.each do |city|
      banners = true if city.banners.any?
    end

    banners
  end


  # Array with cities
  def self.options_with_cities
    state_options = []
    city_options  = []

    self.all.each do |state|
      state.cities.all.each do |city|
        city_options << [city.name, city.id] if city.banners.any?
      end
      state_options << [[state.name, state.id], city_options] if city_options.any?
      city_options = []
    end
    state_options
  end

  # Array without cities
  def self.options_with_banners(category)
    Rails.cache.fetch("options_with_banners_#{category}", expires_in: 12.hours) do
      states_cache = Rails.cache.read('states_cache')
      
      state_options = []
      city_options  = []

      category_id = BannerCategory.find_by(slug: category.parameterize).id if category != 'todos'

      states_cache.all.each do |state|
        state_name = state.name.split('/')[0]
        if category_id
          state_options << [state_name, state.slug] if state.banners.where(banner_category_id: category_id).any?
        else
          state_options << [state_name, state.slug] if state.banners.any?
        end
      end
      state_options
    end
  end

  def cities_with_banners(category)
    Rails.cache.fetch("state_#{self.id}_cities_with_banners_#{category}", expires_in: 12.hours) do
      cities_options = []

      category_id = BannerCategory.find_by(slug: category).id if category != 'todos'

      cities.each do |city|
        if category_id
          cities_options << [city.name, city.slug] if city.banners.where(banner_category_id: category_id).any?
        else
          cities_options << [city.name, city.slug] if city.banners.any?
        end
      end
      cities_options
    end
  end

  # Unused
  def self.grouped_cities
    self.all.map { |state| [state.name, state.cities.map { |city| [city.name, city.id] } ] }
  end

  def categories
    banners.collect{|b| b.category}.uniq
  end

  def canonical?
    cities_cache = Rails.cache.read('cities_cache')
    cities_cache.collect(&:name).include?(self.name)
  end

  # From PostsController#Index
  def self.find_city(state_name, city_name)
    Rails.cache.fetch("state_#{state_name}_city_#{city_name}", expires_in: 30.days) do
      cities_cache = Rails.cache.read('cities_cache')
      city = cities_cache.where(slug: city_name, state_slug: state_name).last
    end
  end

  def self.find_state(state_name)
    Rails.cache.fetch("state_#{state_name}", expires_in: 30.days) do
      states_cache = Rails.cache.read('states_cache')
      state = states_cache.find_by(slug: state_name)
    end
  end

end
