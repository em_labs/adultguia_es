class HtmlText < ActiveRecord::Base
  belongs_to :banner_category
  belongs_to :city
  belongs_to :state

  validates :banner_category, presence: true
  validates :state, presence: true
end
