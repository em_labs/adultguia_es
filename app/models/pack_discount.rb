class PackDiscount < ActiveRecord::Base
  belongs_to :payment_type

  validates :days_from, :days_to, presence: true, numericality: {greater_than: 0}
  validates :discount, presence: true, numericality: {greater_than: 0, less_than: 100}
  validate :days_to_min

  def days_to_min
    return unless days_to && days_from
    errors.add :days_to, "Debe ser mayor que el campo DESDE" if days_to <= days_from
  end

end
