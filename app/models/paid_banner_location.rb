class PaidBannerLocation < ActiveRecord::Base

  belongs_to :banner
  belongs_to :state
  belongs_to :city
  belongs_to :banner_category
  belongs_to :user

  validates :banner, presence: true
  validates :state, presence: true
  validates :city, presence: true
  validates :banner_category, presence: true
  validates :user, presence: true

end
