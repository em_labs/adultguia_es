class PaymentType < ActiveRecord::Base
  acts_as_paranoid

  TYPES = ['subir', 'subir_pack_auto', 'subir_pack_custom', 'destacar', 'top', 'publish']

  has_many :discounts, class_name: "PackDiscount", foreign_key: "payment_type_id"

  validates :code, presence: true, uniqueness: true
  validates :price, presence: true, numericality: {greater_than: 0}
  validates :description, allow_blank: true, length: {maximum: 200}

  accepts_nested_attributes_for :discounts, allow_destroy: true

  scope :non_publish, -> { where.not(code: 'publish') }

  # tipos de pago que aplican para descuento (segun rangos de dias)
  def discount_elegible?
    ['destacar', 'top', 'subir_pack_custom', 'subir_pack_auto'].include?(code)
  end

  # para saber si validar el campo :num_days del payment
  def uses_days?
    ['destacar', 'top', 'subir_pack_auto'].include?(code)
  end

  def get_price(opts={})
    opts.reverse_merge!({
      days: 1
    })

    disc_total = 0
    if discount_elegible?
      disc_total = price * get_discount_percent(opts[:days])
      aplicable_price = (price - disc_total).round(2)
    else
      aplicable_price = price # default base
    end

    (aplicable_price * opts[:days]).round(2)
  end

  def get_discount_percent(days)
    rec = discounts.where(':num_days BETWEEN days_from and days_to OR :num_days > days_to', num_days: days).order('discount desc').first
    rec ? (rec.discount / 100) : 0
  end

  ### CLASS

  # Callback tras hacer el pago successfully, ejecuta la acción de promoción
  # correspondiente
  def self.apply(type_id, banner_id, payment)
    type = PaymentType.find(type_id)
    banner = Banner.find(banner_id)
    case type.code
    # instantaneo
    when "subir"
      # update del campo "sort_date" en banners
      banner.update_column :sort_date, Time.now
    when 'destacar'
      banner.update_column :is_featured, true
      Banner.delay(run_at: payment.expire_at).expire_featured(banner.id)
    # instantaneo, solo programo la expiración en DJ
    when 'top'
      banner.update_column :is_top, true
      Banner.delay(run_at: payment.expire_at).expire_top(banner.id)
    when 'subir_pack_custom'
      # segun las fechas indicadas por el user, las meto tal cual en DJ
      payment.parse_custom_times.each do |date|
        Banner.delay(run_at: date).move_top(banner.id)
      end
    when 'subir_pack_auto'
      # le resto un dia, ya que si lo contrato el 28 de nov, empieza a contabilizar
      # al dia siguiente 29 nov. El ultimo dia es "inclusive" tambien
      start_at = (payment.expire_at - (payment.num_days-1).days).to_date
      end_at = payment.expire_at.to_date
      # creo los jobs en DJ
      (start_at..end_at).to_a.each do |date|
        run_at = Time.parse("#{date.strftime('%Y-%m-%d')} #{payment.schedule_hour}:#{payment.schedule_min}")
        Banner.delay(run_at: run_at).move_top(banner.id)
      end
    end
  end

  def publish_banner?
    code == 'publish'
  end

end
