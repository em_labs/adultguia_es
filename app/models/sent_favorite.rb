class SentFavorite < ActiveRecord::Base
  belongs_to :user
  belongs_to :banner

  validates :user, :banner, presence: true
  validates :email, presence: true, email: true

  after_create :notify

  def notify
    BannerMailer.send_to_friend(self).deliver_later
  end

end
