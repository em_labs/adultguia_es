class AppSetting < ActiveRecord::Base

  validates :key, presence: true
  validates :value, presence: true
  # validates :value_type, presence: true

  CURRENT_COUNTRY = Rails.configuration.x.app_country

  def self.get(lbl)
    rec = self.find_by_key(lbl)
    return '' unless rec
    rec.value
    # case rec.value_type
    # when 'Integer'
    #   rec.value.to_i
    # when 'Boolean'
    #   rec.value == "true"
    # when 'Decimal'
    #   rec.value.to_f
    # else
    #   rec.value
    # end
  end

end