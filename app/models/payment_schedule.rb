class PaymentSchedule < ActiveRecord::Base
  belongs_to :payment

  HOUR_REGEX = /\A([0-9]|0[0-9]|1[0-9]|2[0-3])\z/
  MIN_REGEX = /\A[0-5][0-9]\z/

  # validates :date, presence: true
  validates :time_hour, presence: {message: "introduce una hora"}, format: { with: HOUR_REGEX, message: "hora invalida" }
  validates :time_min, presence: {message: "introduce los minutos"}, format: { with: MIN_REGEX }

  def min_hour
    "#{time_hour}:#{time_min}"
  end

end
