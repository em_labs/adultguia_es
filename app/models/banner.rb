class Banner < ActiveRecord::Base
  acts_as_paranoid
  is_impressionable :counter_cache => true

  extend FriendlyId
  friendly_id :title_seo, use: [:slugged, :history]

  # PHONE_REGEX = /^((\+?34([ \t|\-])?)?[9|6|7]((\d{1}([ \t|\-])?[0-9]{3})|(\d{2}([ \t|\-])?[0-9]{2}))([ \t|\-])?[0-9]{2}([ \t|\-])?[0-9]{2})$/
  PHONE_REGEX = /(([+]?34) ?)?(6(([0-9]{8})|([0-9]{2} [0-9]{6})|([0-9]{2} [0-9]{3} [0-9]{3}))|9(([0-9]{8})|([0-9]{2} [0-9]{6})|([1-9] [0-9]{7})|([0-9]{2} [0-9]{3} [0-9]{3})|([0-9]{2} [0-9]{2} [0-9]{2} [0-9]{2})))/
  
  belongs_to :category, -> { with_deleted }, class_name: "BannerCategory", foreign_key: "banner_category_id", counter_cache: "banners_count"
  belongs_to :user
  belongs_to :state
  belongs_to :city, counter_cache: "banners_count"
  
  has_many :images
  has_many :phone_views, class_name: "BannerPhoneView"
  has_many :contact_requests, class_name: "BannerContactRequest"
  has_many :payments

  has_one :paid_banner_location

  accepts_nested_attributes_for :images, allow_destroy: true

  validates :user, :category, presence: true
  validates :email, presence: true, email: true
  validates :phone, presence: true, numericality: {only_integer: true}, length: { is: Rails.application.secrets.phone_digits }
  validates :price, allow_blank: true, length: {maximum: 40}
  
  validates :title, presence: true, length: {maximum: 100}
  validates :title, uniqueness: { message: "Ya existe un anuncio con el mismo título", case_sensitive: false }
  
  validates :description, presence: true
  validates :description, uniqueness: { message: "Ya existe un anuncio con la misma descripción", case_sensitive: false }
  
  #validate  :desc_presence
  validate  :no_phone_numbers
  # location fields
  validates :state, :city, presence: true
  
  #validate :check_similar_title
  #validate :check_similar_description

  default_scope -> { order('sort_date desc, id desc') }
  default_scope -> { where(validated: true) }

  scope :top, -> { where(is_top: true) }
  scope :non_deleted, -> { where(deleted_at: nil) }
  scope :visible, -> { where('visible_from is null OR visible_from <= ?', Time.now) }
  scope :is_valid, -> { where(validated: true) }

  before_create do
    self.sort_date = Time.zone.now if sort_date.blank?
  end
  before_validation do
    # raise description.inspect
    opts = {
      elements: ['b','strong','ul','li','ol','i','br','p','span','style'],
      attributes: {'span'=>['style']},
      css: {
        properties: ['font-weight','font-style']
      }
    }
    self.description = Sanitize.fragment(description, opts)
    # raise description.inspect
  end

  before_create do
    loop do
      uuid = SecureRandom.uuid
      unless Banner.exists?(validation_uuid: uuid)
        self.validation_uuid = uuid
        break
      end
    end
  end

  def no_phone_numbers
    errors.add :description, "No puedes incluir números de teléfono" if PHONE_REGEX.match(description)
    errors.add :title, "No puedes incluir números de teléfono" if PHONE_REGEX.match(title)
    errors.add :price, "No puedes incluir números de teléfono" if PHONE_REGEX.match(price)
  end

  def should_generate_new_friendly_id?
    title_changed? || new_record?
  end

  def title_seo
    "#{title} (#{location_name} - #{phone})"
  end

  def location_name
    str = ""
    str << "#{city.name}, " if city
    str << state.name if state
    str
  end

  def css_classes
    arr = []
    arr << 'featured' if is_featured?
    arr << 'top' if is_top?
    arr.join(' ')
  end

  def desc_presence
    errors.add :description, "Debes introducir un mensaje" if description.blank?
  end

  def phone_digits
    Rails.application.secrets.phone_digits
  end

  def formatted_phone(telf = nil)
    number = telf ? telf : phone
    out = ''
    # ES (xxx-xxx-xxx_)
    number.split('').each_slice(3) {|g| out << g.join("") + " " }
    if phone_digits == 10
      # formato MX (xxx-xxx-xxxx)
      a = out.split(' ')
      out = "#{a[0]} #{a[1]} #{a[2]}#{a[3]}"
    end
    out
  end

  # formato tipo 962 938 XXX
  def hidden_phone
    suffix = phone_digits = 9 ? 'XXX' : 'XXXX'
    telf = phone[0..-4] + suffix
    formatted_phone(telf)
  end

  def next
    scope = Banner.where(is_top: is_top?, category: self.category, city: self.city).where.not(id: self.id)
    scope.where("sort_date < ?", sort_date).first
  end

  def prev
    scope = Banner.where(is_top: is_top?, category: self.category, city: self.city).where.not(id: self.id)
    scope.where("sort_date > ?", sort_date).first
  end

  def has_payment_location?
    paid_banner_location.present?
  end

  def check_similar_title
    if Banner.check_similar_string(user_id, title, 'title', 10)
      errors.add(:title, "Ya existe un anuncio con un título muy similar.")
    end
  end
  
  def check_similar_description
    if Banner.check_similar_string(user_id, description, 'description', 35)
      errors.add(:description, "Ya existe un anuncio con una descripción muy similar.")
    end
  end


  def self.check_similar_string(user_id, string, field_name, percent)
    user    = User.find user_id
    banners = user.banners
    
    field_strings = banners.collect(&:title)       if field_name == 'title'
    field_strings = banners.collect(&:description) if field_name == 'description'
    
    similars = 0

    field_strings.each do |field_string|
      similar_percent = field_string.similar(string).to_i
      puts "#{string} - #{similar_percent}% - #{field_string} "
      if similar_percent >= percent
        similars += 1
      end
    end

    puts "Existen #{similars} anuncios con un título muy similar al tuyo"

    true if similars > 0
  end

  # Dandelion API
  def self.compare_texts(text1, text2)
    begin
      ascii_text1 = ActiveSupport::Inflector.transliterate(text1)
      ascii_text2 = ActiveSupport::Inflector.transliterate(text2)
      uri = "https://api.dandelion.eu/datatxt/sim/v1/?text1=#{ascii_text1}&text2=#{ascii_text2}&lang=es&token=dd46c5159f894c1d814409bdc9abbc7e"
      response = HTTP.get(uri)
      similarity = response["similarity"]
      (similarity * 100).to_i
    rescue => e
      false
    end
  end

  ### CLASS

  # async job para expirar el banner "destacado" cuando llegue su fecha
  # de caducidad (expire_at)
  def self.expire_featured(banner_id)
    b = Banner.find(banner_id)
    b.update_column :is_featured, false
  end

  def self.expire_top(banner_id)
    b = Banner.find(banner_id)
    b.update_column :is_top, false
  end

  def self.move_top(banner_id)
    b = Banner.find(banner_id)
    b.update_column :sort_date, Time.now
  end

  def self.recommended
    Rails.cache.fetch("recommended_banners", expires_in: 12.hours) do
      recommended_posts = self.unscoped.is_valid.non_deleted.where(recommended: true)
      others_posts      = self.unscoped.is_valid.non_deleted.includes(:images, :category, :state, :city).order('impressions_count desc').limit(5)

      recommended_posts + others_posts
    end
  end
end
