class BannerCategory < ActiveRecord::Base
  acts_as_paranoid
  
  extend FriendlyId
  friendly_id :title, use: [:slugged, :history]

  has_many :banners
  has_many :html_texts
  has_many :paid_banner_locations

  validates :title, presence: true

  def should_generate_new_friendly_id?
    title_changed? || new_record?
  end

end