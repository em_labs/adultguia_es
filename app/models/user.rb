class User < ActiveRecord::Base
  acts_as_paranoid

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable, :recoverable, :rememberable, :trackable, :validatable

  has_many :banners
  has_many :payments, through: :banners
  has_many :favorites, class_name: "UserFavorite", foreign_key: "user_id"
  has_many :sent_favorites, class_name: "SentFavorite", foreign_key: "user_id"
  has_many :paid_banner_locations

  belongs_to :state
  belongs_to :city

  #validates :first_name, :last_name, presence: true
  validates :state, :city, presence: true

  after_destroy :delete_banners

  def delete_banners
    banners.destroy_all
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def location_name
    return "" unless city && state
    "#{city.name}, #{state.name}"
  end
  
end
