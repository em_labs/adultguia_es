class City < ActiveRecord::Base
  belongs_to :state
  has_many :banners
  has_many :paid_banner_locations

  before_save :set_slug

  default_scope -> { order('name asc') }

  def set_slug
    self.slug = name.parameterize
  end

  def categories
    banners.collect{|b| b.category}.uniq
  end

  def with_state
    "#{self.state_name} - #{self.name}"
  end

  def canonical?
    states_cache = Rails.cache.read('states_cache')
    states_cache.collect(&:name).include?(self.name)
  end

end
