class Payment < ActiveRecord::Base

  belongs_to :banner, -> { with_deleted }
  belongs_to :payment_type, -> { with_deleted }
  
  has_many :schedules, class_name: "PaymentSchedule", foreign_key: "payment_id"

  serialize :paypal_response
  serialize :aplicable_days_of_week

  DAYS_OF_WEEK = {
    'L'=>'1',
    'M'=>'2',
    'X'=>'3',
    'J'=>'4',
    'V'=>'5',
    'S'=>'6',
    'D'=>'0',
  }

  ### VALIDATION

  # base
  validates :banner, :payment_type, presence: true
  validates :price, presence: true, numericality: true
  validates :state, presence: true, inclusion: {in: ['cart','complete']}
  # optional
  validates :num_days, presence: true, numericality: {greater_than: 0}, if: " payment_type.uses_days? "
  validates :expire_at, presence: true, if: " state == 'complete' && payment_type.uses_days? "

  # pack custom
  validate :one_schedule_min

  # pack auto
  with_options if: " payment_type.code == 'subir_pack_auto' " do |a|
    a.validates :schedule_hour, presence: true, format: { with: PaymentSchedule::HOUR_REGEX }
    a.validates :schedule_min, presence: true, format: { with: PaymentSchedule::MIN_REGEX }
  end

  accepts_nested_attributes_for :schedules, allow_destroy: true

  ### SCOPES

  scope :complete, -> { where(state: 'complete') }
  # donde el servicio expire y aun no hayamos llegado a la expiración O
  # el payment sea one-time sin caducidad (como "destacar")
  scope :active, -> { complete.where(
    '(expire_at is not null and ? < expire_at) OR (payment_type_id = ?)',
    Time.now,
    PaymentType.find_by(code: 'destacar').id
    )
  }

  ### CALLBACKS

  before_validation do
    self.state = 'cart' if new_record?
  end
  before_create :set_expiration

  def set_expiration
    if payment_type.code == "subir_pack_custom" 
      last_date = schedules.map(&:run_at).sort().last
      self.expire_at = last_date
    end
  end

  def one_schedule_min
    if payment_type.code == "subir_pack_custom"
      errors.add :time_errors, "Selecciona al menos 1 hora" if schedules.empty?
      errors.add :time_errors, "Hay horas repetidas en tu selección" if schedules.map(&:min_hour).uniq!
      errors.add :schedule_errors, 'El numero de times != num_days' if parse_custom_times.size != num_days
      errors.add :start_date, 'El numero de times != num_days' if start_date <= Date.current
    end
  end

  ### OTHER

  def parse_custom_times
    jobs = []
    count = 0
    hours = schedules.map(&:min_hour).sort
    day = start_date
    num_days.times do |i|
      hours.each do |hh|
        if !day.wday.to_s.in?(aplicable_days_of_week)
          ap "skipping day #{day} / #{day.wday}"
          next
        end
        jobs << Time.parse("#{day.strftime('%Y-%m-%d')} #{hh}")
        count += 1
        break if count == num_days
      end
      break if count == num_days
      day = day + 1.day
    end
    jobs
  end

  def complete?
    state == 'complete'
  end

  def process(data)
    self.paypal_response = data
    if data.ack == "Success"
      self.state = 'complete'
      # seteamos fecha de expiración si este tipo de pago usa dias
      if payment_type.uses_days?
        self.expire_at = num_days.days.from_now
      end
      if payment_type.code == 'subir_pack_custom'
        self.expire_at = parse_custom_times.last
      end
    end
    save!

    # apply promo action
    if complete?
      PaymentType.apply(payment_type.id, banner_id, self)
    end
  end

end