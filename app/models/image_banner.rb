class ImageBanner < ActiveRecord::Base
  mount_uploader :desktop_image, ImageBannerDesktopUploader
  mount_uploader :mobile_image, ImageBannerMobileUploader

  belongs_to :banner_category
  belongs_to :city
  belongs_to :state

  validates :banner_category, presence: true
  #validates :state, presence: true
  validates :desktop_image, presence: true
  validates :mobile_image, presence: true
  validates :desktop_link, presence: true
  validates :mobile_link, presence: true

  scope :available, -> { where("start_date IS NOT NULL AND end_date IS NOT NULL AND ? BETWEEN start_date AND end_date", Time.now)}

  def available?
    if active 
      if start_date && end_date && Range.new(start_date.to_date, end_date.to_date).include?(Time.now.to_date) #(start_date..end_date).include?(Time.now)
        true
      elsif !start_date && !end_date
        true
      end
    else
      false
    end
  end

  def date_range
    "#{start_date} - #{end_date}" if start_date && end_date
  end
end
