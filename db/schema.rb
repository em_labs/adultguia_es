# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180414202533) do

  create_table "app_settings", force: :cascade do |t|
    t.string "key",   limit: 255
    t.text   "value", limit: 65535
  end

  add_index "app_settings", ["key"], name: "index_app_settings_on_key", using: :btree

  create_table "banner_categories", force: :cascade do |t|
    t.string   "title",         limit: 255
    t.string   "slug",          limit: 255
    t.datetime "deleted_at"
    t.integer  "banners_count", limit: 4,   default: 0
  end

  add_index "banner_categories", ["slug"], name: "index_banner_categories_on_slug", unique: true, using: :btree

  create_table "banner_contact_requests", force: :cascade do |t|
    t.integer  "banner_id",    limit: 4
    t.string   "name",         limit: 255
    t.string   "email",        limit: 255
    t.string   "phone",        limit: 255
    t.text     "body",         limit: 65535
    t.string   "ip_address",   limit: 255
    t.string   "session_hash", limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "banner_contact_requests", ["banner_id"], name: "index_banner_contact_requests_on_banner_id", using: :btree

  create_table "banner_phone_views", force: :cascade do |t|
    t.integer  "banner_id",    limit: 4
    t.string   "ip_address",   limit: 255
    t.string   "session_hash", limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "banner_phone_views", ["banner_id"], name: "index_banner_phone_views_on_banner_id", using: :btree

  create_table "banners", force: :cascade do |t|
    t.integer  "user_id",            limit: 4
    t.integer  "banner_category_id", limit: 4
    t.string   "title",              limit: 255
    t.string   "email",              limit: 255
    t.string   "phone",              limit: 255
    t.text     "description",        limit: 65535
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.datetime "deleted_at"
    t.integer  "impressions_count",  limit: 4,     default: 0
    t.string   "slug",               limit: 255
    t.datetime "sort_date"
    t.string   "geo_lat",            limit: 255
    t.string   "geo_lon",            limit: 255
    t.string   "geo_address",        limit: 255
    t.string   "geo_place_id",       limit: 255
    t.string   "geo_keywords",       limit: 255
    t.string   "price",              limit: 255
    t.boolean  "is_featured",                      default: false
    t.boolean  "is_top",                           default: false
    t.integer  "favorites_count",    limit: 4,     default: 0
    t.boolean  "alerts",                           default: true
    t.string   "meta_title",         limit: 255
    t.string   "meta_desc",          limit: 255
    t.integer  "state_id",           limit: 4
    t.integer  "city_id",            limit: 4
    t.datetime "visible_from"
    t.boolean  "recommended",                      default: false
    t.boolean  "paid",                             default: true
    t.boolean  "validated",                        default: true
    t.string   "validation_uuid",    limit: 255
  end

  add_index "banners", ["banner_category_id"], name: "index_banners_on_banner_category_id", using: :btree
  add_index "banners", ["city_id"], name: "index_banners_on_city_id", using: :btree
  add_index "banners", ["geo_keywords"], name: "index_banners_on_geo_keywords", using: :btree
  add_index "banners", ["slug"], name: "index_banners_on_slug", unique: true, using: :btree
  add_index "banners", ["state_id"], name: "index_banners_on_state_id", using: :btree
  add_index "banners", ["user_id"], name: "index_banners_on_user_id", using: :btree

  create_table "cities", force: :cascade do |t|
    t.integer  "state_id",      limit: 4
    t.string   "code",          limit: 255
    t.string   "control_digit", limit: 255
    t.string   "name",          limit: 255
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "slug",          limit: 255
    t.integer  "banners_count", limit: 4,   default: 0
    t.string   "state_name",    limit: 255
    t.string   "state_slug",    limit: 255
  end

  add_index "cities", ["banners_count"], name: "index_cities_on_banners_count", using: :btree
  add_index "cities", ["code"], name: "index_cities_on_code", using: :btree
  add_index "cities", ["control_digit"], name: "index_cities_on_control_digit", using: :btree
  add_index "cities", ["slug"], name: "index_cities_on_slug", using: :btree
  add_index "cities", ["state_id"], name: "index_cities_on_state_id", using: :btree

  create_table "comunidades", force: :cascade do |t|
    t.string  "slug",       limit: 50,  null: false
    t.string  "comunidad",  limit: 255, null: false
    t.integer "capital_id", limit: 4,   null: false
  end

  add_index "comunidades", ["comunidad"], name: "IDX_cominidad", unique: true, using: :btree
  add_index "comunidades", ["slug"], name: "slug", unique: true, using: :btree

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   limit: 4,     default: 0, null: false
    t.integer  "attempts",   limit: 4,     default: 0, null: false
    t.text     "handler",    limit: 65535,             null: false
    t.text     "last_error", limit: 65535
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by",  limit: 255
    t.string   "queue",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "external_banners", force: :cascade do |t|
    t.text "es_mobile",  limit: 65535
    t.text "es_desktop", limit: 65535
    t.text "mx_mobile",  limit: 65535
    t.text "mx_desktop", limit: 65535
  end

  create_table "feedback_requests", force: :cascade do |t|
    t.string   "email",      limit: 255
    t.text     "message",    limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",           limit: 255, null: false
    t.integer  "sluggable_id",   limit: 4,   null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope",          limit: 255
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "html_texts", force: :cascade do |t|
    t.integer  "banner_category_id", limit: 4
    t.integer  "city_id",            limit: 4
    t.integer  "state_id",           limit: 4
    t.text     "html_block",         limit: 65535
    t.boolean  "active",                           default: true
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "html_texts", ["banner_category_id"], name: "index_html_texts_on_banner_category_id", using: :btree
  add_index "html_texts", ["city_id"], name: "index_html_texts_on_city_id", using: :btree
  add_index "html_texts", ["state_id"], name: "index_html_texts_on_state_id", using: :btree

  create_table "image_banners", force: :cascade do |t|
    t.integer  "banner_category_id", limit: 4
    t.integer  "city_id",            limit: 4
    t.integer  "state_id",           limit: 4
    t.string   "desktop_image",      limit: 255
    t.string   "mobile_image",       limit: 255
    t.text     "desktop_link",       limit: 65535
    t.text     "mobile_link",        limit: 65535
    t.boolean  "all_locations",                    default: false
    t.datetime "start_date"
    t.datetime "end_date"
    t.boolean  "active",                           default: true
  end

  create_table "images", force: :cascade do |t|
    t.integer  "banner_id",  limit: 4
    t.string   "title",      limit: 255
    t.string   "alt",        limit: 255
    t.integer  "position",   limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "file",       limit: 255
    t.string   "width",      limit: 255
    t.string   "height",     limit: 255
  end

  add_index "images", ["banner_id"], name: "index_images_on_banner_id", using: :btree

  create_table "impressions", force: :cascade do |t|
    t.string   "impressionable_type", limit: 255
    t.integer  "impressionable_id",   limit: 4
    t.integer  "user_id",             limit: 4
    t.string   "controller_name",     limit: 255
    t.string   "action_name",         limit: 255
    t.string   "view_name",           limit: 255
    t.string   "request_hash",        limit: 255
    t.string   "ip_address",          limit: 255
    t.string   "session_hash",        limit: 255
    t.text     "message",             limit: 65535
    t.text     "referrer",            limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "impressions", ["controller_name", "action_name", "ip_address"], name: "controlleraction_ip_index", using: :btree
  add_index "impressions", ["controller_name", "action_name", "request_hash"], name: "controlleraction_request_index", using: :btree
  add_index "impressions", ["controller_name", "action_name", "session_hash"], name: "controlleraction_session_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "ip_address"], name: "poly_ip_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "request_hash"], name: "poly_request_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "session_hash"], name: "poly_session_index", using: :btree
  add_index "impressions", ["impressionable_type", "message", "impressionable_id"], name: "impressionable_type_message_index", length: {"impressionable_type"=>nil, "message"=>255, "impressionable_id"=>nil}, using: :btree
  add_index "impressions", ["user_id"], name: "index_impressions_on_user_id", using: :btree

  create_table "municipios", force: :cascade do |t|
    t.integer "provincia_id", limit: 4,   null: false
    t.string  "municipio",    limit: 255, null: false
    t.string  "slug",         limit: 255, null: false
    t.float   "latitud",      limit: 53
    t.float   "longitud",     limit: 53
  end

  add_index "municipios", ["provincia_id", "municipio"], name: "IDX_municipio", unique: true, using: :btree
  add_index "municipios", ["slug"], name: "slug", unique: true, using: :btree

  create_table "pack_discounts", force: :cascade do |t|
    t.integer "payment_type_id", limit: 4
    t.string  "title",           limit: 255
    t.integer "days_from",       limit: 4
    t.integer "days_to",         limit: 4
    t.decimal "discount",                    precision: 11, scale: 2
  end

  add_index "pack_discounts", ["payment_type_id"], name: "index_pack_discounts_on_payment_type_id", using: :btree

  create_table "paid_banner_locations", force: :cascade do |t|
    t.integer "banner_id",          limit: 4
    t.integer "state_id",           limit: 4
    t.integer "city_id",            limit: 4
    t.integer "banner_category_id", limit: 4
    t.integer "user_id",            limit: 4
  end

  create_table "payment_schedules", force: :cascade do |t|
    t.integer  "payment_id", limit: 4
    t.date     "date"
    t.string   "time_hour",  limit: 255
    t.string   "time_min",   limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.datetime "run_at"
  end

  add_index "payment_schedules", ["payment_id"], name: "index_payment_schedules_on_payment_id", using: :btree

  create_table "payment_types", force: :cascade do |t|
    t.string   "title",       limit: 255
    t.string   "code",        limit: 255
    t.decimal  "price",                     precision: 11, scale: 2
    t.text     "description", limit: 65535
    t.datetime "deleted_at"
    t.boolean  "active",                                             default: true
  end

  add_index "payment_types", ["code"], name: "index_payment_types_on_code", using: :btree

  create_table "payments", force: :cascade do |t|
    t.string   "state",                  limit: 255
    t.integer  "banner_id",              limit: 4
    t.integer  "payment_type_id",        limit: 4
    t.integer  "num_days",               limit: 4
    t.decimal  "price",                                precision: 11, scale: 2
    t.decimal  "total",                                precision: 11, scale: 2
    t.decimal  "discount_percent",                     precision: 11, scale: 2
    t.datetime "expire_at"
    t.datetime "created_at",                                                    null: false
    t.datetime "updated_at",                                                    null: false
    t.text     "paypal_response",        limit: 65535
    t.string   "schedule_hour",          limit: 255
    t.string   "schedule_min",           limit: 255
    t.text     "aplicable_days_of_week", limit: 65535
    t.date     "start_date"
  end

  add_index "payments", ["banner_id"], name: "index_payments_on_banner_id", using: :btree
  add_index "payments", ["payment_type_id"], name: "index_payments_on_payment_type_id", using: :btree

  create_table "provincias", force: :cascade do |t|
    t.string  "slug",         limit: 50,               null: false
    t.string  "provincia",    limit: 255,              null: false
    t.integer "comunidad_id", limit: 4,                null: false
    t.integer "capital_id",   limit: 4,   default: -1, null: false
  end

  add_index "provincias", ["comunidad_id"], name: "FK_provincias", using: :btree
  add_index "provincias", ["provincia"], name: "IDX_provincia", unique: true, using: :btree

  create_table "sent_favorites", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "banner_id",  limit: 4
    t.string   "email",      limit: 255
    t.text     "message",    limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "sent_favorites", ["banner_id"], name: "index_sent_favorites_on_banner_id", using: :btree
  add_index "sent_favorites", ["user_id"], name: "index_sent_favorites_on_user_id", using: :btree

  create_table "sitemap_cities", force: :cascade do |t|
    t.string "name", limit: 255
  end

  create_table "states", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "country",    limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "slug",       limit: 255
  end

  add_index "states", ["country"], name: "index_states_on_country", using: :btree
  add_index "states", ["slug"], name: "index_states_on_slug", using: :btree

  create_table "super_users", force: :cascade do |t|
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
  end

  add_index "super_users", ["email"], name: "index_super_users_on_email", unique: true, using: :btree
  add_index "super_users", ["reset_password_token"], name: "index_super_users_on_reset_password_token", unique: true, using: :btree

  create_table "user_favorites", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "banner_id",  limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "user_favorites", ["banner_id"], name: "index_user_favorites_on_banner_id", using: :btree
  add_index "user_favorites", ["user_id"], name: "index_user_favorites_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "provider",               limit: 255
    t.string   "uid",                    limit: 255
    t.datetime "deleted_at"
    t.string   "geo_lat",                limit: 255
    t.string   "geo_lon",                limit: 255
    t.string   "geo_address",            limit: 255
    t.string   "geo_place_id",           limit: 255
    t.string   "unconfirmed_email",      limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "confirmation_token",     limit: 255
    t.integer  "state_id",               limit: 4
    t.integer  "city_id",                limit: 4
  end

  add_index "users", ["city_id"], name: "index_users_on_city_id", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", using: :btree
  add_index "users", ["provider"], name: "index_users_on_provider", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["state_id"], name: "index_users_on_state_id", using: :btree
  add_index "users", ["uid"], name: "index_users_on_uid", using: :btree

  add_foreign_key "banner_contact_requests", "banners"
  add_foreign_key "banner_phone_views", "banners"
  add_foreign_key "banners", "banner_categories"
  add_foreign_key "banners", "cities"
  add_foreign_key "banners", "states"
  add_foreign_key "banners", "users"
  add_foreign_key "cities", "states"
  add_foreign_key "html_texts", "banner_categories"
  add_foreign_key "html_texts", "cities"
  add_foreign_key "html_texts", "states"
  add_foreign_key "images", "banners"
  add_foreign_key "pack_discounts", "payment_types"
  add_foreign_key "payment_schedules", "payments"
  add_foreign_key "payments", "banners"
  add_foreign_key "payments", "payment_types"
  add_foreign_key "sent_favorites", "banners"
  add_foreign_key "sent_favorites", "users"
  add_foreign_key "user_favorites", "banners"
  add_foreign_key "user_favorites", "users"
  add_foreign_key "users", "cities"
  add_foreign_key "users", "states"
end
