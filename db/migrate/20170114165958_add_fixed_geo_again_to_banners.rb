class AddFixedGeoAgainToBanners < ActiveRecord::Migration
  def change
    add_reference :banners, :state, index: true, foreign_key: true
    add_reference :banners, :city, index: true, foreign_key: true
  end
end
