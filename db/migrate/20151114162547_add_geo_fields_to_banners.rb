class AddGeoFieldsToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :geo_lat, :string
    add_column :banners, :geo_lon, :string
  end
end
