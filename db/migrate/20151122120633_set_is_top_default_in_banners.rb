class SetIsTopDefaultInBanners < ActiveRecord::Migration
  def change
    change_column :banners, :is_top, :boolean, default: false
  end
end
