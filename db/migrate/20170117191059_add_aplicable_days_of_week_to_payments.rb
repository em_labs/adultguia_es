class AddAplicableDaysOfWeekToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :aplicable_days_of_week, :text
    add_column :payments, :start_date, :date
  end
end
