class RemoveGeoAreaCountryFromUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.remove :geo_area
      t.remove :geo_country
    end
  end
end