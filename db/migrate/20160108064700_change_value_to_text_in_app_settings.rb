class ChangeValueToTextInAppSettings < ActiveRecord::Migration
  def change
    change_column :app_settings, :value, :text
  end
end
