class CreateFeedbackRequests < ActiveRecord::Migration
  def change
    create_table :feedback_requests do |t|
      t.string :email
      t.text :message

      t.timestamps null: false
    end
  end
end
