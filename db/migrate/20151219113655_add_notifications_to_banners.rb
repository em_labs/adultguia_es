class AddNotificationsToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :alerts, :boolean, default: true
  end
end
