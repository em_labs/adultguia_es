class RemoveBannerCountFromBannerCategories < ActiveRecord::Migration
  def change
    remove_column :banner_categories, :banner_count, :integer
  end
end
