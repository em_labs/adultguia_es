class AddDeletedAtToPaymentTypes < ActiveRecord::Migration
  def change
    add_column :payment_types, :deleted_at, :datetime
  end
end
