class AddListDateToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :list_date, :date
  end
end
