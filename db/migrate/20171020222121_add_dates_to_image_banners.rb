class AddDatesToImageBanners < ActiveRecord::Migration
  def change
    add_column :image_banners, :start_date, :datetime
    add_column :image_banners, :end_date,   :datetime
    add_column :image_banners, :active,     :boolean, default: true
  end
end
