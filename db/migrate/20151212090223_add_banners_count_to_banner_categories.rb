class AddBannersCountToBannerCategories < ActiveRecord::Migration
  def change
    add_column :banner_categories, :banners_count, :integer, default: 0
  end
end
