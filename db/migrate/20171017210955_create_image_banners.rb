class CreateImageBanners < ActiveRecord::Migration
  def change
    create_table :image_banners do |t|
      t.integer :banner_category_id
      t.integer :city_id
      t.integer :state_id
      t.string :desktop_image
      t.string :mobile_image
    end
  end
end
