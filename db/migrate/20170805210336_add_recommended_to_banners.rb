class AddRecommendedToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :recommended, :boolean, default: false
  end
end
