class AddLinksToBanners < ActiveRecord::Migration
  def change
    add_column :image_banners, :desktop_link, :string
    add_column :image_banners, :mobile_link,  :string
    add_column :image_banners, :all_locations, :boolean, default: false
  end
end
