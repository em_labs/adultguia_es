class AdDescriptionToPaymentTypes < ActiveRecord::Migration
  def change
    add_column :payment_types, :description, :text
  end
end
