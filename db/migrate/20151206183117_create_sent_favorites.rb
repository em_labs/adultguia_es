class CreateSentFavorites < ActiveRecord::Migration
  def change
    create_table :sent_favorites do |t|
      t.references :user, index: true, foreign_key: true
      t.references :banner, index: true, foreign_key: true
      t.string :email
      t.text :message

      t.timestamps null: false
    end
  end
end
