class CreatePackDiscounts < ActiveRecord::Migration
  def change
    create_table :pack_discounts do |t|
      t.references :payment_type, index: true, foreign_key: true
      t.string :title
      t.integer :days_from
      t.integer :days_to
      t.decimal :discount, precision: 11, scale: 2

      t.timestamps null: false
    end
  end
end
