class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.references :banner, index: true, foreign_key: true
      t.references :payment_type, index: true, foreign_key: true
      t.integer :num_days
      t.decimal :price, precision: 11, scale: 2
      t.decimal :total, precision: 11, scale: 2
      t.decimal :discount_percent, precision: 11, scale: 2
      t.datetime :expire_at

      t.timestamps null: false
    end
  end
end
