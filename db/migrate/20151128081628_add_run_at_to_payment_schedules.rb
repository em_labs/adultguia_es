class AddRunAtToPaymentSchedules < ActiveRecord::Migration
  def change
    add_column :payment_schedules, :run_at, :datetime
  end
end
