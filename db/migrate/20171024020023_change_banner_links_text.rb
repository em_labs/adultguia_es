class ChangeBannerLinksText < ActiveRecord::Migration
  def change
    change_column :image_banners, :desktop_link, :text
    change_column :image_banners, :mobile_link,  :text
  end
end
