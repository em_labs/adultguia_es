class AddPaypalResponseToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :paypal_response, :text
  end
end
