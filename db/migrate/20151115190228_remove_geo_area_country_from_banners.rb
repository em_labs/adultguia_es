class RemoveGeoAreaCountryFromBanners < ActiveRecord::Migration
  def change
    change_table :banners do |t|
      t.remove :geo_area
      t.remove :geo_country
    end
  end
end
