class CreateCitiesAlt < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.references :state, index: true, foreign_key: true
      t.string :code, index: true
      t.string :control_digit, index: true
      t.string :name

      t.timestamps null: false
    end
  end
end
