class AddSlugToStatesCities < ActiveRecord::Migration
  def change
    add_column :states, :slug, :string
    add_column :cities, :slug, :string
    add_index :cities, :slug
    add_index :states, :slug
  end
end
