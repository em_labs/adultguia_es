class CreateBannerContactRequests < ActiveRecord::Migration
  def change
    create_table :banner_contact_requests do |t|
      t.references :banner, index: true, foreign_key: true
      t.string :name
      t.string :email
      t.string :phone
      t.text :body
      t.string :ip_address
      t.string :session_hash

      t.timestamps null: false
    end
  end
end
