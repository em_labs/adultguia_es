class AddGeoKeywordsToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :geo_keywords, :string, index: true
  end
end
