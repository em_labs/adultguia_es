class AddValidationToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :validated, :boolean, default: true
    add_column :banners, :validation_uuid, :string
  end
end
