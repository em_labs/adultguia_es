class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.references :banner_category, index: true, foreign_key: true
      t.references :province, index: true, foreign_key: true
      t.references :city, index: true, foreign_key: true
      t.string :title
      t.string :email
      t.string :phone
      t.text :description

      t.timestamps null: false
    end
  end
end
