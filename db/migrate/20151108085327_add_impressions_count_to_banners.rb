class AddImpressionsCountToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :impressions_count, :integer, default: 0
  end
end
