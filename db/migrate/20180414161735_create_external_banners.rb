class CreateExternalBanners < ActiveRecord::Migration
  def change
    create_table :external_banners do |t|
      t.text :es_mobile
      t.text :es_desktop
      t.text :mx_mobile
      t.text :mx_desktop
    end

    es_mobile_script = '<iframe src="//ads.exosrv.com/iframe.php?idzone=2973204&size=300x100" width="300" height="100" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>'
    mx_mobile_script = '<iframe src="//ads.exosrv.com/iframe.php?idzone=2973208&size=300x100" width="300" height="100" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>'
    es_desktop_script = '<iframe src="//ads.exosrv.com/iframe.php?idzone=2973202&size=300x250" width="300" height="250" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>'
    mx_desktop_script = '<iframe src="//ads.exosrv.com/iframe.php?idzone=2973206&size=315x300" width="315" height="300" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>'

    ExternalBanner.create(
      es_mobile:  es_mobile_script,
      mx_mobile:  mx_mobile_script,
      es_desktop: es_desktop_script,
      mx_desktop: mx_desktop_script
    )
  end
end
