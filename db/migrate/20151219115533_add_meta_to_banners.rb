class AddMetaToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :meta_title, :string
    add_column :banners, :meta_desc, :string
  end
end
