class AddLocationToUsers < ActiveRecord::Migration
  def change
    add_column :users, :location_name, :string
    add_reference :users, :province, index: true, foreign_key: true
    add_reference :users, :city, index: true, foreign_key: true
  end
end
