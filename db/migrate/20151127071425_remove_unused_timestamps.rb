class RemoveUnusedTimestamps < ActiveRecord::Migration
  def change
    change_table :pack_discounts do |t|
      t.remove :created_at
      t.remove :updated_at
    end 
    change_table :payment_types do |t|
      t.remove :created_at
      t.remove :updated_at
    end 
    change_table :banner_categories do |t|
      t.remove :created_at
      t.remove :updated_at
    end 
  end
end
