class AddLocationNameToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :location_name, :string
  end
end
