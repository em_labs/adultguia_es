class AddSlugToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :slug, :string
    add_index :banners, :slug, unique: true
  end
end
