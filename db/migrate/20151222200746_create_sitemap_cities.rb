class CreateSitemapCities < ActiveRecord::Migration
  def change
    create_table :sitemap_cities do |t|
      t.string :name
    end
  end
end
