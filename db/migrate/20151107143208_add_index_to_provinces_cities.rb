class AddIndexToProvincesCities < ActiveRecord::Migration
  def change
    add_index :provinces, :name
    add_index :cities, :name
  end
end
