class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.references :banner, index: true, foreign_key: true
      t.string :title
      t.string :alt
      t.integer :position

      t.timestamps null: false
    end
  end
end
