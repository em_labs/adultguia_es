class AddIndexToBannersGeoKeywords < ActiveRecord::Migration
  def change
    add_index :banners, :geo_keywords
  end
end
