class AddBannersCountToCities < ActiveRecord::Migration
  def change
    add_column :cities, :banners_count, :integer, default: 0
    add_index :cities, :banners_count
  end
end
