class AddIsFeaturedToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :is_featured, :boolean, default: false
  end
end
