class AddFavsCountToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :favorites_count, :integer, default: 0
  end
end
