class ChangeListDateToDatetime < ActiveRecord::Migration
  def change
    change_table :banners do |t|
      t.remove :list_date
      t.datetime :sort_date
    end
  end
end
