class AddPriceToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :price, :string
  end
end
