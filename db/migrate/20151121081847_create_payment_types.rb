class CreatePaymentTypes < ActiveRecord::Migration
  def change
    create_table :payment_types do |t|
      t.string :title
      t.string :code, index: true
      t.decimal :price, precision: 11, scale: 2

      t.timestamps null: false
    end
  end
end
