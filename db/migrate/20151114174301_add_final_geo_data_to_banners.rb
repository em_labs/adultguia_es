class AddFinalGeoDataToBanners < ActiveRecord::Migration
  def change
    # pks
    remove_reference :banners, :province, index: true, foreign_key: true
    remove_reference :banners, :city, index: true, foreign_key: true
    
    change_table :banners do |t|
      t.string :geo_address
      t.string :geo_area, index: true
      t.string :geo_country, index: true
      t.string :geo_place_id, index: true
    end
  end
end
