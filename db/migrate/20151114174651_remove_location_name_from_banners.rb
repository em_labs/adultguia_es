class RemoveLocationNameFromBanners < ActiveRecord::Migration
  def change
    remove_column :banners, :location_name, :string
  end
end
