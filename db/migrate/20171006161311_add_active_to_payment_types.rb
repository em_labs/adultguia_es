class AddActiveToPaymentTypes < ActiveRecord::Migration
  def change
    add_column :payment_types, :active, :boolean, default: true

    PaymentType.create(
      title:  'Subir Anuncio',
      code:   'publish',
      price:  1.0,
      active: true,
      description: 'Precio por subir un anuncio'
    )
  end
end
