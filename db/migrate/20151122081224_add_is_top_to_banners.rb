class AddIsTopToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :is_top, :boolean
  end
end
