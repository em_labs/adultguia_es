class CreatePaymentSchedules < ActiveRecord::Migration
  def change
    create_table :payment_schedules do |t|
      t.references :payment, index: true, foreign_key: true
      t.date :date
      t.string :time_hour
      t.string :time_min

      t.timestamps null: false
    end
  end
end
