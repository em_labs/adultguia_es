class AddVisibleFromToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :visible_from, :datetime
  end
end
