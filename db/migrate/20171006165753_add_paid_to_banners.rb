class AddPaidToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :paid, :boolean, default: true
  end
end
