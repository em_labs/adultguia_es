class AddGeoFieldsToUsers < ActiveRecord::Migration
  def change
    remove_reference :users, :province, index: true, foreign_key: true
    remove_reference :users, :city, index: true, foreign_key: true
    change_table :users do |t|
      t.remove :location_name
      # new fields
      t.string :geo_lat
      t.string :geo_lon
      t.string :geo_address, index: true
      t.string :geo_area, index: true
      t.string :geo_country, index: true
      t.string :geo_place_id, index: true
    end
  end
end