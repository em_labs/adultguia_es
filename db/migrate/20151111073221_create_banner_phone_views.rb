class CreateBannerPhoneViews < ActiveRecord::Migration
  def change
    create_table :banner_phone_views do |t|
      t.references :banner, index: true, foreign_key: true
      t.string :ip_address
      t.string :session_hash

      t.timestamps null: false
    end
  end
end
