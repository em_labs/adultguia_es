class AddDeletedAtToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :deleted_at, :datetime
  end
end
