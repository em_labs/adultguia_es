class AddStateInfoToCities < ActiveRecord::Migration
  def change
    add_column :cities, :state_name, :string
    add_column :cities, :state_slug, :string

    city_count = City.count
    City.all.each_with_index do |city, index|
      puts "Updating City #{index+1} of #{city_count}"
      city.state_name = city.state.name
      city.state_slug = city.state.slug
      city.save
    end
  end
end
