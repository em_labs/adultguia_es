class AddUserToBanners < ActiveRecord::Migration
  def change
    add_reference :banners, :user, index: true, foreign_key: true, after: :id
  end
end
