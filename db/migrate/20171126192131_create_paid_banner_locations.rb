class CreatePaidBannerLocations < ActiveRecord::Migration
  def change
    create_table :paid_banner_locations do |t|
      t.references :banner
      t.references :state
      t.references :city
      t.references :banner_category
      t.references :user
    end
  end
end
