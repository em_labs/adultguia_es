class AddDeletedAtToBannerCategories < ActiveRecord::Migration
  def change
    add_column :banner_categories, :deleted_at, :datetime
  end
end
