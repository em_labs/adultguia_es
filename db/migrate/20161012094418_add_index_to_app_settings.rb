class AddIndexToAppSettings < ActiveRecord::Migration
  def change
    add_index :app_settings, :key
  end
end
