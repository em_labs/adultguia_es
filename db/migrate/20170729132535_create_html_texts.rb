class CreateHtmlTexts < ActiveRecord::Migration
  def change
    create_table :html_texts do |t|
      t.references :banner_category, index: true, foreign_key: true
      t.references :city, index: true, foreign_key: true
      t.references :state, index: true, foreign_key: true
      t.text :html_block
      t.boolean :active, default: true

      t.timestamps null: false
    end
  end
end
