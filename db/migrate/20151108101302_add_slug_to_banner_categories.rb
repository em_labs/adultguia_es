class AddSlugToBannerCategories < ActiveRecord::Migration
  def change
    add_column :banner_categories, :slug, :string
    add_index :banner_categories, :slug, unique: true
  end
end