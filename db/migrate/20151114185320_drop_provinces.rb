class DropProvinces < ActiveRecord::Migration
  def change
    drop_table :cities
    drop_table :provinces
  end
end
