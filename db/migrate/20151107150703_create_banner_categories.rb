class CreateBannerCategories < ActiveRecord::Migration
  def change
    create_table :banner_categories do |t|
      t.string :title
      t.integer :banner_count, default: 0

      t.timestamps null: false
    end
  end
end
