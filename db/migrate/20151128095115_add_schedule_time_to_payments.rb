class AddScheduleTimeToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :schedule_hour, :string
    add_column :payments, :schedule_min, :string
  end
end
